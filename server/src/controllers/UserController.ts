import { Document, Schema, Model, model, Types } from 'mongoose';
import { Request, Response, NextFunction } from 'express';
import { Trainer } from '@/models/Trainer';
import { User, BaseUser } from '@/models/User';
import { Plan } from '@/models/Plan';
import TrainersController from './TrainersController';
import bcrypt from 'bcryptjs';
import { Exercise } from '@/models/Exercise';
import { Achievement } from '@/models/Achievement';
import NotificationManager from './NotificationManager';
import { Notification } from '@/models/Notification';

export default abstract class UserController {

	public static USER_ACHIEVEMENT_LOOKUP = {
		$lookup: {
			from: 'achievements',
			localField: 'achievements',
			foreignField: 'achievementId',
			as: 'achievements',
		},
	};

	public static parseUser(user: User) {
		const userInfo = {
			_id: user._id,
			email: user.email,
			firstName: user.firstName,
			lastName: user.lastName,
			sex: user.sex,
			birthdate: user.birthdate,
			address: user.address,
			profileImage: user.profileImage,
			level: user.level,
			experience: user.experience,
			achievements: user.achievements,
			exercises: user.exercises,
		};
		return userInfo;
	}

	public static getSortedUsers(sortQuery: any) {
		return (req: Request, res: Response, next: NextFunction) => {

			const callback = (err: any, users: any[]) => {
				if (err) {
					// console.log(err);
					res.status(404).send({
						description: 'User not found, the requested _id seems incorrect',
					});
				} else {
					if (users == null || users.length === 0) {
						res.status(404).send({
							description: 'User not found',
						});
					} else {
						const parsedUsers = users.map((u) => this.parseUser(u));
						res.status(200).json(parsedUsers);
					}
				}
			};

			if (sortQuery) {
				User.aggregate([
					sortQuery,
					this.USER_ACHIEVEMENT_LOOKUP,
				]).exec(callback);
			} else {
				User.aggregate([
					this.USER_ACHIEVEMENT_LOOKUP,
				]).exec(callback);
			}
		};
	}

	public static getUsers() {
		return this.getSortedUsers(null);
	}

	public static getLeaderboardUsers() {
		return this.getSortedUsers({ $sort: { level: -1, experience: -1 } });
	}

	public static getUser() {
		return (req: Request, res: Response, next: NextFunction) => {
			User.aggregate([
				{ $match: { _id: new Types.ObjectId(req.params.id) } },
				this.USER_ACHIEVEMENT_LOOKUP,
			]).exec((err: any, users: any[]) => {
				if (err) {
					// console.log(err);
					res.status(404).send({
						description: 'User not found, the requested _id seems incorrect',
					});
				} else {
					if (users == null || users.length === 0) {
						res.status(404).send({
							description: 'User not found',
						});
					} else {
						const parsedUser = this.parseUser(users[0]);
						res.status(200).json(parsedUser);
					}
				}
			});
		};
	}

	public static getUserInfo() {
		return (req: Request, res: Response, next: NextFunction) => {
			if (!req.user) {
				// console.log('User info not passed to getUserInfo()');
				res.status(404).send({
					description: 'User not found, the requested _id seems incorrect',
				});
				return;
			}
			// console.log(req.user);
			// Retrieving user id
			const userId = req.user._id;
			User.aggregate([
				{ $match: { _id: new Types.ObjectId(userId) } },
				this.USER_ACHIEVEMENT_LOOKUP,
			]).exec((err: any, users: any[]) => {
				if (err) {
					// console.log(err);
					res.status(404).send({
						description: 'User not found, the requested _id seems incorrect',
					});
				} else {
					if (users == null || users.length === 0) {
						res.status(404).send({
							description: 'User not found',
						});
					} else {
						const parsedUser = this.parseUser(users[0]);
						res.status(200).json(parsedUser);
					}
				}
			});
		};
	}

	public static postUpdateUserInfo() {
		return this.postUpdateUser(false);
	}

	public static postUpdateTrainerInfo() {
		return this.postUpdateUser(true);
	}

	public static postUpdateUser(isTrainer: boolean) {
		return (req: Request, res: Response, next: NextFunction) => {
			if (!req.user) {
				// console.log('User info not passed to postUpdateUserInfo()');
				res.status(404).send({
					description: (isTrainer ? 'Trainer' : 'User') + ' not found, the requested _id seems incorrect',
				});
				return;
			}
			// console.log(req.user);
			// Retrieving user id
			const userId = req.user._id;

			this.manageUserUpdate(req, res, userId, isTrainer);
		};
	}

	public static manageUserUpdate(req: Request, res: Response, userId: any, isTrainer: boolean) {
		// Checking body fields
		// console.log(req.body);
		if (!req.body) {
			res.status(500).json({
				description: 'Request body has not been set',
			});
			return;
		}

		if (req.body.email) {
			res.status(500).json({
				description: 'The ' + (isTrainer ? 'trainer' : 'user') + ' cannot modify its email',
			});
			return;
		}

		// Parsing new credentials
		const updateCredentials = this.parseUpdateCredentials(req.body, isTrainer);

		const callback = (err: any, user: any) => {
			if (err) {
				// console.log(err);
				res.status(404).send({
					description: 'Error while updating ' + (isTrainer ? 'trainer' : 'user') + ' info',
				});
				return;
			}

			res.status(200).send({
				description: (isTrainer ? 'Trainer' : 'User') + ' info updated successfully',
			});
		};

		const newPassword = req.body.password;
		const oldPassword = req.body.oldPassword;

		if (!newPassword) {
			// User does not want to change password, we update the credentials
			if (isTrainer) {
				Trainer.findByIdAndUpdate(userId, updateCredentials, callback);
			} else {
				User.findByIdAndUpdate(userId, updateCredentials, callback);
			}
		} else {
			if (!oldPassword) {
				// The user wants to update the password but has not provided the old one
				res.status(404).send({
					description: 'Old password has not been provided (field oldPassword)',
				});
				return;
			}
			// We must check the previous password

			const passwordCallback = (user: any) => {
				if (!user) {
					res.status(404).send({
						description: 'User not found, the requested _id seems incorrect',
					});
					return;
				}

				const userSalt = user.salt;
				const userPassword = user.password;

				// Hash requires password, salt and the callback called when the has has been generated
				bcrypt.hash(oldPassword, userSalt, (hashError, hash) => {
					if (hashError) {
						// console.log(hashError);
						res.status(400).json({
							description: 'Error while generating hash',
						});
						return;
					}

					// console.log(hash);
					// console.log(userPassword);

					// Check if the password are the same
					if (userPassword !== hash) {
						res.status(400).json({
							description: 'The old password does not match',
						});
						return;
					}

					// The password are the same, the user can update it
					// We need to generate a new hashed password
					bcrypt.genSalt(10, (saltErr, salt) => {
						if (saltErr) {
							// console.log(saltErr);
							res.status(400).json({
								description: 'Error while generating salt',
							});
							return;
						}

						// Hash requires password, salt and the callback called when the has has been generated
						bcrypt.hash(newPassword, salt, (newHashError, newHash) => {
							if (newHashError) {
								// console.log(newHashError);
								res.status(400).json({
									description: 'Error while generating new hash',
								});
								return;
							}

							updateCredentials.password = newHash;
							updateCredentials.salt = salt;

							if (isTrainer) {
								Trainer.findByIdAndUpdate(userId, updateCredentials, callback);
							} else {
								User.findByIdAndUpdate(userId, updateCredentials, callback);
							}
						});

					});
				});
			};

			const catchPassword = (passwordError: any) => {
				// console.log(passwordError);
				res.status(404).send({
					description: 'User not found, the requested _id seems incorrect',
				});
			};

			if (isTrainer) {
				Trainer.findById(userId).then(passwordCallback).catch(catchPassword);
			} else {
				User.findById(userId).then(passwordCallback).catch(catchPassword);
			}

		}

	}

	public static postRequestPlan() {
		return (req: Request, res: Response, next: NextFunction) => {
			if (!req.user) {
				// console.log('User info not passed to getUserInfo()');
				res.status(404).send({
					description: 'User not found, the requested _id seems incorrect',
				});
				return;
			}

			// Retrieving user id
			const userId = req.user._id;

			// Checking body fields
			// console.log(req.body);
			if (!req.body) {
				res.status(500).json({
					description: 'Request body has not been set',
				});
				return;
			}

			if (!req.body.trainerId) {
				res.status(400).json({
					description: 'Trainer id is missing',
				});
				return;
			}

			const newPlan = new Plan({
				associatedTo: userId,
				compiledBy: req.body.trainerId,
				description: req.body.description,
				timetable: req.body.timetable,
				muscles: req.body.muscles,
				trainerTags: req.body.trainerTags,
			});

			newPlan.save((err, plan) => {
				if (err) {
					// console.log(err);
					res.status(404).send({
						description: 'Error while creating the plan request',
					});
					return;
				}

				if (plan) {
					// We must send a notification to the trainer
					// tslint:disable-next-line: max-line-length
					const notification = NotificationManager.sendNewPlanRequestNotification(newPlan.compiledBy._id, userId, newPlan._id);
					Notification.create(notification).then((savedNotification) => {
						if (savedNotification) {
							// console.log('Sent and saved notification (NewPlanRequest)');
							// console.log(savedNotification);
						}
					});

					res.status(200).json({
						description: 'Plan requested successfully',
					});
				}
			});
		};
	}

	public static getUserPlans() {
		return (req: Request, res: Response, next: NextFunction) => {
			if (!req.user) {
				// console.log('User info not passed to getUserInfo()');
				res.status(404).send({
					description: 'User not found, the requested _id seems incorrect',
				});
				return;
			}

			// Retrieving user id
			const userId = req.user._id;
			const matchQuery = {
				$match: {
					associatedTo: new Types.ObjectId(userId),
				},
			};

			Plan.aggregate([
				matchQuery,
				TrainersController.TRAINER_TRAINERTAGS_LOOKUP,
				TrainersController.TRAINER_MUSCLES_LOOKUP,
			]).exec((err, plans: any[]) => {
				if (err) {
					// console.log(err);
					res.status(404).send({
						description: 'Error while retrieving user plans',
					});
					return;
				}

				res.status(200).send(plans);
			});
		};
	}

	public static postRatePlan() {
		return (req: Request, res: Response, next: NextFunction) => {
			if (!req.user) {
				// console.log('User info not passed to postRatePlan()');
				res.status(404).send({
					description: 'User not found, the requested _id seems incorrect',
				});
				return;
			}

			// Checking body fields
			// console.log(req.body);
			if (!req.body) {
				res.status(500).json({
					description: 'Request body has not been set',
				});
				return;
			}

			if (!req.body.planId || !req.body.rating) {
				res.status(400).json({
					description: 'planId or rating fields are missing',
				});
				return;
			}

			const planId = req.body.planId;
			const rating = req.body.rating;

			if (rating <= 0 || rating > 5) {
				res.status(400).json({
					description: 'Rating cannot be <= 0 or > 5',
				});
				return;
			}

			// Retrieving user id
			const userId = req.user._id;
			Plan.find({
				_id: new Types.ObjectId(planId),
				associatedTo: new Types.ObjectId(userId),
			})
				.then((plans: any[]) => {
					if (!plans || plans.length === 0) {
						// No plan found
						res.status(404).send({
							description: 'Error while retrieving requested plan, planId seems incorrect',
						});
						return;
					}

					const requestedPlan = plans[0];
					const trainerId = requestedPlan.compiledBy;
					// console.log('FOUND PLAN');
					// console.log(requestedPlan);

					// Setting the plan rating
					requestedPlan.rating = rating;
					requestedPlan.isRated = true;

					requestedPlan.save((saveErr: any, savedPlan: any) => {
						if (saveErr) {
							// console.log(saveErr);
							res.status(404).send({
								description: 'Error while saving requested plan with updated rating',
							});
							return;
						}

						// console.log('SAVED PLAN RATING: ' + rating);

						// Updating trainer mean rating
						Plan.find({
							compiledBy: new Types.ObjectId(trainerId),
							isRated: true,
						})
							.then((ratedPlans: any[]) => {
								if (ratedPlans && ratedPlans.length > 0) {
									const safePlans = ratedPlans.filter((p) => p.rating > 0 && p.rating <= 5);
									// console.log(safePlans);

									if (safePlans && safePlans.length > 0) {
										const numPlans = safePlans.length;
										// console.log(safePlans[0].rating);

										const sumRating = safePlans.map((p) => p.rating).reduce((a, b) => a + b, 0);

										// console.log(numPlans);
										// console.log(sumRating);

										// Mean trainer value
										const meanTrainerRating = Math.round(sumRating / numPlans);

										// console.log('COMPUTED MEAN TRAINER RATING: ' + meanTrainerRating);

										Trainer.findById(trainerId).then((trainer) => {
											if (trainer) {
												trainer.rating = meanTrainerRating;
												trainer.save();
												// console.log('SAVED MEAN TRAINER RATING: ' + meanTrainerRating);
											}

											res.status(200).send({
												description: 'Plan rated successfully',
											});
										});
									}
								}
							})
							.catch((err) => {
								if (err) {
									// console.log(err);
									res.status(404).send({
										description: 'Error while retrieving requested plan',
									});
									return;
								}
							});
					});

				})
				.catch((err) => {
					if (err) {
						// console.log(err);
						res.status(404).send({
							description: 'Error while retrieving requested plan',
						});
						return;
					}
				});

		};
	}

	public static postCompleteExercise() {
		return (req: Request, res: Response, next: NextFunction) => {
			if (!req.user) {
				// console.log('User info not passed to postCompleteExercise()');
				res.status(404).send({
					description: 'User not found, the requested _id seems incorrect',
				});
				return;
			}
			// Checking body fields
			// console.log(req.body);
			if (!req.body) {
				res.status(500).json({
					description: 'Request body has not been set',
				});
				return;
			}

			if (!req.body.exercises || !req.body.newExperience || !req.body.newLevel) {
				res.status(400).json({
					description: 'exercises, newExperience or newLevel fields are missing',
				});
				return;
			}

			const exercises: any[] = req.body.exercises;
			const exercisesIds = exercises.map((ex) => ex.exerciseId);
			const newExperience = req.body.newExperience;
			const newLevel = req.body.newLevel;

			// console.log(exercises);
			// console.log(exercisesIds);
			// console.log(newExperience);
			// console.log(newLevel);

			// Retrieving user id
			const userId = req.user._id;

			User.findById(userId)
				.then((result: any) => {
					if (!result) {
						// No user found
						res.status(404).send({
							description: 'Error while retrieving the user, userId seems incorrect',
						});
						return;
					}

					const user: User = result;

					// Retriving saved exercises
					Exercise.find({ exerciseId: { $in: exercisesIds } })
						.then((foundExercises: Exercise[]) => {
							if (!foundExercises || foundExercises.length === 0) {
								res.status(404).send({
									description: 'Could not find any of the requested exercises',
								});
								return;
							}

							// Retrieving current user exercises
							const userExercises: any[] = user.exercises;
							const updatedExercises: any[] = [];
							// We must update the exercises
							exercises.forEach((ex: any) => {
								const exerciseId = ex.exerciseId;
								let amount = ex.amount;

								// Retrieve exercise between the saved ones
								const foundEx = foundExercises.find((fex) => fex.exerciseId === exerciseId);
								if (foundEx) {
									// We must check if the exercise already exists. If it does, we must update the amount
									const foundUserEx = userExercises.find((uex) => uex.exercise.exerciseId === exerciseId);
									if (foundUserEx) {
										// The exercise already exists, we update the amount
										amount += foundUserEx.amount;
									}
									// Saving the new exercise in the array
									updatedExercises.push({
										exercise: foundEx,
										amount,
									});
								}
							});

							// Preserve the exercises that were not updated
							userExercises
								.filter(((uex) => !exercisesIds.includes(uex.exercise.exerciseId)))
								.forEach((uex) => updatedExercises.push(uex));
							// Update the user
							user.exercises = updatedExercises;
							user.level = newLevel;
							user.experience = newExperience;

							// Checking Achievements
							Achievement.find({}).then((achievements: Achievement[]) => {
								const userAchievements: any[] = user.achievements;
								const newAchievements: any[] = [];
								if (achievements && achievements.length > 0) {
									achievements.forEach((ac: Achievement) => {
										const acCard: any = ac.card;
										const foundExerciseForAchievement = user.exercises.find((uex) => uex.exercise.exerciseId === acCard.exercise);
										if (foundExerciseForAchievement && !userAchievements.includes(ac.achievementId)) {
											// console.log('CHECKING AMOUNT FOR ACHIEVEMENT: ' + ac.achievementId);
											// Checking the amount, if the user does not already have the achievement
											if (foundExerciseForAchievement.amount >= acCard.amount) {
												// Add achievement
												userAchievements.push(ac.achievementId);
												newAchievements.push(ac);
												// console.log('ADD ACHIEVEMENT: ' + ac.achievementId);
											}
										}
									});
								}

								// Setting the achievements
								user.achievements = userAchievements;

								user.save((saveErr, savedUser) => {
									if (saveErr) {
										// Error while saving user
										// console.log(saveErr);
										res.status(404).send({
											description: 'Error while saving the use after updating its exercises',
										});
										return;
									}
									const parsedUser = this.parseUser(user);
									const finalRes: any = {};
									finalRes.user = parsedUser;
									finalRes.newAchievements = newAchievements;
									res.status(200).send(finalRes);
								});
							});
						})
						.catch((err) => {
							if (err) {
								res.status(404).send({
									description: 'Error while retrieving the exercises',
								});
								return;
							}
						});
				})
				.catch((err) => {
					if (err) {
						res.status(404).send({
							description: 'Error while retrieving the user',
						});
						return;
					}
				});
		};
	}

	public static updateUserInfo() {
		return this.updateUser(false);
	}

	public static updateTrainerInfo() {
		return this.updateUser(true);
	}

	public static updateUser(isTrainer: boolean) {
		return (req: Request, res: Response, next: NextFunction) => {
			// console.log(req.body);
			if (!req.body || !req.params.id) {
				res.status(404).send({
					description: 'Must specify an id in path params',
				});
				return;
			}

			// console.log(req.params.id);
			// Retrieving user id
			const userId = req.params.id;

			this.manageUserUpdate(req, res, userId, isTrainer);
		};
	}

	public static deleteUser() {
		return (req: Request, res: Response) => {
			// console.log(req.body);
			if (!req.params.id) {
				res.status(404).send({
					description: 'Must specify an id in path params',
				});
				return;
			}

			const userId = req.params.id;
			// console.log(userId);

			User.findOneAndDelete({
				_id: new Types.ObjectId(userId),
			}, (err, oldUser) => {
				if (err || !oldUser) {
					// console.log(err);
					// console.log(oldUser);
					res.status(404).send({
						description: 'Error while deleting the user',
					});
					return;
				}

				res.status(200).send({ description: 'User deleted successfully' });
			});
		};
	}

	private static parseUpdateCredentials(credentials: any, isTrainer: boolean) {
		const updatedCredentials: any = {};
		if (credentials.firstName) {
			updatedCredentials.firstName = credentials.firstName;
		}
		if (credentials.lastName) {
			updatedCredentials.lastName = credentials.lastName;
		}
		if (credentials.sex != null && credentials.sex !== undefined) {
			updatedCredentials.sex = credentials.sex;
		}
		if (credentials.address) {
			updatedCredentials.address = credentials.address;
		}
		if (credentials.birthdate) {
			updatedCredentials.birthdate = credentials.birthdate;
		}
		if (credentials.profileImage) {
			updatedCredentials.profileImage = credentials.profileImage;
		}

		if (isTrainer) {
			if (credentials.muscles) {
				updatedCredentials.muscles = credentials.muscles;
			}
			if (credentials.trainerTags) {
				updatedCredentials.trainerTags = credentials.trainerTags;
			}
		}

		return updatedCredentials;
	}
}
