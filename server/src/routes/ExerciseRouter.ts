import { Application } from 'express';
import ExerciseController from '@/controllers/ExerciseController';

export default abstract class ExerciseRouter {
	public static setupRoutes(app: Application) {
        app.route('/api/exercises')
            .get(ExerciseController.getExercises())
            .post(ExerciseController.createExercise());

        app.route('/api/exercises/:exerciseId')
            .put(ExerciseController.updateExercise())
            .delete(ExerciseController.deleteExercise());
    }
}
