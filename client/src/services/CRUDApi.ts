import Api from '@/services/Api';
import Product from '@/models/Product';
import Exercise from '@/models/Exercise';
import Achievement from '@/models/Achievement';
import { UserWithId, UpdatedUser } from '@/models/User';

export default {
  // _______________________PRODUCTS_______________________________________________________
  putNewProduct(newProduct: Product) {
      return Api.withAdminAuthorization().post('/api/shop/products', {
        categoryId: newProduct.categoryId,
        name: newProduct.name,
        price: newProduct.price,
        imageOne: newProduct.imageOne,
        imageTwo: newProduct.imageTwo,
        onSale: newProduct.onSale,
        discountedPrice: newProduct.discountedPrice,
        description: newProduct.description,
        exercises: newProduct.exercises.map((exer) => exer.exerciseId),
      },
      );
  },
  
  updateProduct(newProduct: Product) {
    return Api.withAdminAuthorization().put('/api/shop/products/' + newProduct.productId, {
      categoryId: newProduct.categoryId,
      name: newProduct.name,
      price: newProduct.price,
      imageOne: newProduct.imageOne,
      imageTwo: newProduct.imageTwo,
      onSale: newProduct.onSale,
      discountedPrice: newProduct.discountedPrice,
      description: newProduct.description,
      exercises: newProduct.exercises.map((exer) => exer.exerciseId),
    },
    );
  },

  deleteProduct(productId: string) {
    return Api.withoutAuthorization().delete('/api/shop/products/' + productId);
  },
  
  // _______________________USERS_______________________________________________________
  deleteUser(userId: string) {
    return Api.withAdminAuthorization().delete('/api/users/' + userId);
  },

  updateUser(user: UpdatedUser) {
    return Api.withAdminAuthorization().put('/api/users/' + user._id, user);
  },

  // _______________________MUSCLES_______________________________________________________
  deleteMuscle(selectedMuscleId: number) {
    return Api.withAdminAuthorization().delete('/api/muscles/' + selectedMuscleId);
  },

  addMuscle(muscleName: string) {
    return Api.withAdminAuthorization().post('/api/muscles', {
      name: muscleName,
    },
    );
  },

  updateMuscle(muscleId: number, muscleName: string) {
    return Api.withAdminAuthorization().put('/api/muscles/' + muscleId, {
        name: muscleName,
    },
    );
  },

  // _______________________ACHIEVEMENTS_______________________________________________________
  getAchivement() {
    return Api.withoutAuthorization().get('/api/achievements');
  },

  deleteAchievement(selectedAchievementId: number) {
    return Api.withAdminAuthorization().delete('/api/achievements/' + selectedAchievementId);
  },

  putNewAchievement(newAchievement: Achievement) {
    return Api.withAdminAuthorization().post('/api/achievements', {
      name: newAchievement.name,
      description: newAchievement.description,
      icon: newAchievement.icon,
      amount: newAchievement.card.amount,
      exercise: newAchievement.card.exercise.exerciseId,
    },
    );
  },

  updateAchievement(newAchievement: Achievement) {
    return Api.withAdminAuthorization().put('/api/achievements/' + newAchievement.achievementId, {
      name: newAchievement.name,
      description: newAchievement.description,
      icon: newAchievement.icon,
      amount: newAchievement.card.amount,
      exercise: newAchievement.card.exercise.exerciseId,
    },
    );
  },

  // _______________________EXERCISES_______________________________________________________
  deleteExercise(selectedExerciseId: number) {
    return Api.withAdminAuthorization().delete('/api/exercises/' + selectedExerciseId);
  },
  putNewExercise(newExercise: Exercise) {
    return Api.withAdminAuthorization().post('/api/exercises', {
      name: newExercise.name,
      description: newExercise.description,
      isTimed: newExercise.isTimed,
      isMapNeeded: newExercise.isMapNeeded,
      isWeightNeeded: newExercise.isWeightNeeded,
      muscles: newExercise.muscles.map((muscle) => muscle.muscleId),
      exerciseImage: newExercise.exerciseImage,
    },
    );
  },
  updateExercise(newExercise: Exercise) {
    console.log(newExercise);
    return Api.withAdminAuthorization().put('/api/exercises/' + newExercise.exerciseId, {
      name: newExercise.name,
      description: newExercise.description,
      isTimed: newExercise.isTimed,
      isMapNeeded: newExercise.isMapNeeded,
      isWeightNeeded: newExercise.isWeightNeeded,
      muscles: newExercise.muscles.map((muscle) => muscle.muscleId),
      exerciseImage: newExercise.exerciseImage,
    },
    );
  },
};
