import { Document, Schema, Model, model } from 'mongoose';

export interface Category extends Document {
    categoryId: number;
    name: string;
}

const CategorySchema: Schema = new Schema({
    categoryId: { type: Number, unique: true, required: true },
    name: { type: String, required: true },
});

// Export the Category model and return the Category interface
export const Category: Model<Category> = model<Category>('Category', CategorySchema);
