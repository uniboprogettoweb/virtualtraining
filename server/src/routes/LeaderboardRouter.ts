import { Application } from 'express';
import UserController from '@/controllers/UserController';

export default abstract class LeaderboardRouter {
    /* tslint:disable */
	public static setupRoutes(app: Application) {
		app.route('/api/leaderboard')
			.get(UserController.getLeaderboardUsers());
	}
}
