import { Document, Schema, Model, model } from 'mongoose';
import { Request, Response } from 'express';
import { Achievement } from '@/models/Achievement';
import ExerciseController from './ExerciseController';
import { isContext } from 'vm';

export default abstract class AchievementController {

    public static ACHIEVEMENT_EXERCISE_LOOKUP = {
		$lookup: {
			from: 'exercises',
			localField: 'card.exercise',
			foreignField: 'exerciseId',
			as: 'card.exercise',
        },
    };

    public static ACHIEVEMENT_EXERCISE_MUSCLES_LOOKUP = {
		$lookup: {
			from: 'muscles',
			localField: 'card.exercise.muscles',
			foreignField: 'muscleId',
			as: 'card.exercise.muscles',
		},
    };

    public static getAchievements() {
		return (req: Request, res: Response) => {
            Achievement.aggregate([
                this.ACHIEVEMENT_EXERCISE_LOOKUP,
                {
                    $unwind: '$card.exercise',
                },
                this.ACHIEVEMENT_EXERCISE_MUSCLES_LOOKUP,
            ]).exec((err: any, achievements: Achievement[]) => {
                if (err) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Achievements not found',
                    });
                    return;
                }

                res.status(200).send(achievements);
            });
		};
    }

    public static createAchievement() {
		return (req: Request, res: Response) => {
            // console.log(req.body);
            if (!req.body
                || !req.body.name
                || !req.body.description
                || !req.body.icon
                || !req.body.amount
                || !req.body.exercise) {
                res.status(404).send({
                    description: 'Must specify name, description, icon, amount and exercise',
                });
                return;
            }

            Achievement.find().sort({achievementId: -1}).limit(1).exec((err: any, achievements: Achievement[]) => {
                if (err) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Error while creating a new achievement',
                    });
                    return;
                }

                let newAchievementId = 1;
                if (achievements.length !== 0) {
                    newAchievementId = achievements[0].achievementId + 1;
                }

                const newAchievement = new Achievement({
                    achievementId: newAchievementId,
                    name: req.body.name,
                    description: req.body.description,
                    icon: req.body.icon,
                    card: {
                        amount: req.body.amount,
                        exercise: req.body.exercise,
                    },
                });

                newAchievement.save((createErr: any, createdAchievement: Achievement) => {
                    if (createErr) {
                        // console.log(createErr);
                        res.status(404).send({
                            description: 'Error while creating a new exercise',
                        });
                        return;
                    }

                    res.status(201).send(createdAchievement);
                });
            });
        };
    }

    public static updateAchievement() {
		return (req: Request, res: Response) => {
            // console.log(req.body);
            if (!req.body
                || !req.body.name
                || !req.body.description
                || !req.body.icon
                || !req.body.amount
                || !req.body.exercise
                || !req.params.achievementId) {
                res.status(404).send({
                    // tslint:disable-next-line: max-line-length
                    description: 'Must specify an achievementId in path params and name, description, icon, amount, exercise in body',
                });
                return;
            }

            const achievementId = req.params.achievementId;
            const name = req.body.name;
            const description = req.body.description;
            const icon = req.body.icon;
            const amount = req.body.amount;
            const exercise = req.body.exercise;

            Achievement.findOneAndUpdate({
                achievementId,
            }, {
                name,
                description,
                icon,
                amount,
                exercise,
            }, {
                upsert: false,
            }, (err, oldAchievement) => {
                if (err || !oldAchievement) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Error while updating the achievement',
                    });
                    return;
                }

                res.status(200).send({
                    _id: oldAchievement._id,
                    achievementId: oldAchievement.achievementId,
                    name,
                    description,
                    icon,
                    amount,
                    exercise,
                });
            });
		};
    }

    public static deleteAchievement() {
		return (req: Request, res: Response) => {
            if (!req.params.achievementId) {
                res.status(404).send({
                    description: 'Must specify an achievementId in path params',
                });
                return;
            }

            const achievementId = req.params.achievementId;

            Achievement.findOneAndDelete({
                achievementId,
            }, (err, oldAchievement) => {
                if (err || !oldAchievement) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Error while deleting the achievement',
                    });
                    return;
                }

                res.status(200).send({description: 'Achievement deleted successfully'});
            });
		};
    }
}
