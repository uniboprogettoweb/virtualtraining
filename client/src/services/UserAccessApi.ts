import Api from '@/services/Api';
import { NewUser } from '@/models/User';

export default {
  postRegister(newUser: NewUser) {
    return (newUser.isACoach) ? 
    Api.withoutAuthorization().post('/api/register-trainer', newUser) :
    Api.withoutAuthorization().post('/api/register', newUser);
  },
  postLoginUser(email: string, password: string) {
    return Api.withoutAuthorization().post('/api/login', {email, password});
  },
  postLoginCoach(email: string, password: string) {
    return Api.withoutAuthorization().post('/api/login-trainer', {email, password});
  },
  postLoginAdmin(email: string, password: string) {
    return Api.withoutAuthorization().post('/api/login-admin', {email, password});
  },
  postLogoutUser() {
    return Api.withAuthorization().post('/api/logout');
  },
  postLogoutTrainer() {
    return Api.withTrainerAuthorization().post('/api/logout');
  },
  postLogoutAdmin() {
    return Api.withAdminAuthorization().post('/api/logout');
  },
};
