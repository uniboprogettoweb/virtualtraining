import Api from '@/services/Api';
import { Muscle } from '@/services/UserRequestApi';

export interface Exercise {
  exerciseId: number;
  name: string;
  description: string;
  isTimed: boolean;
  muscles: Muscle[];
}

export default {

  getReqList() {
    return Api.withoutAuthorization().get('/api/requestList');
  },
  getMuscleList() {
    return Api.withoutAuthorization().get('/api/muscles');
  },
  getExList() {
    return Api.withoutAuthorization().get('/api/exercises');
  },

  getCoachInfo() {
    return Api.withoutAuthorization().get('/api/coachInfo');
  },

};
