import { Document, Schema, Model, model } from 'mongoose';
import { User } from './User';
import { CartProduct } from './CartProduct';

export interface Cart extends Document {
    userId: User;
    products: CartProduct[];
    price: number;
    discountedPrice: number;
}

const CartSchema: Schema = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: User, required: true},
    products: { type: Array, required: true, default: []},
    price: { type: Number, required: true, default: 0.0 },
    discountedPrice: { type: Number, default: null },
});

// Export the Cart model and return the Cart interface
export const Cart: Model<Cart> = model<Cart>('Cart', CartSchema);
