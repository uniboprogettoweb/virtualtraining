import { Document, Schema, Model, model } from 'mongoose';
import { Request, Response, NextFunction } from 'express';

import data_muscoli from '@/mock/muscoli.json';
import data_requests from '@/mock/request.json';
import data_esercizi from '@/mock/esercizi.json';

export default abstract class CoachRequestsController {


  public static requestList() {
		return (req: Request, res: Response, next: NextFunction) => {
			res.json(data_requests);
		};
	}
}
