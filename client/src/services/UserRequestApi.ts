import Api from '@/services/Api';

export interface Muscle {
	muscleId: number;
  name: string;
}


export default {
  getMuscles() {
    return Api.withoutAuthorization().get('/api/muscles');
  },
	getTrainers() {
    return Api.withoutAuthorization().get('/api/trainers');
  },
  getUsers() {
    return Api.withoutAuthorization().get('/api/users');
  },
  getAdmins() {
    return Api.withoutAuthorization().get('/api/admins');
  },
};
