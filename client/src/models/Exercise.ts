import Muscle from './Muscle';

export default interface Exercise {
    exerciseId: number;
    name: string;
    description: string;
    isTimed: boolean;
    isMapNeeded: boolean;
    isWeightNeeded: boolean;
    muscles: Muscle[];
    exerciseImage: string;
  }
