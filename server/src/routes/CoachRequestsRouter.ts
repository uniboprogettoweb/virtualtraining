import { Application } from 'express';
import CoachRequestsController from '@/controllers/CoachRequestsController';


export default abstract class CoachRequestsRouter {
	public static setupRoutes(app: Application) {

    app.route('/api/requestList')
    .get(CoachRequestsController.requestList());
	}
}
