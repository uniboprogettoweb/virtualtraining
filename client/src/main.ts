import Vue from 'vue';
import VueCookies from 'vue-cookies';
import { CredentialsUtils } from './Utils';
import BootstrapVue from 'bootstrap-vue';
import VueGeolocation from 'vue-browser-geolocation';
import { LMap, LTileLayer, LMarker } from 'vue2-leaflet';
import { Icon, Util } from 'leaflet';
import 'leaflet/dist/leaflet.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import App from './App.vue';
import router from './router';
import socketIO from 'socket.io-client';
import { utils } from 'mocha';
import EventBus from 'vue-bus-ts';
import TrainersApi from './services/TrainersApi';
import UserApi from './services/UserApi';



Vue.use(BootstrapVue);
Vue.use(VueCookies);
Vue.use(VueGeolocation);
Vue.use(EventBus);
Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);
delete (Icon.Default.prototype as any)._getIconUrl;

Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});

// Cookies max age
Vue.cookies.config('1d');

Vue.config.productionTip = false;
Vue.use(BootstrapVue);

// Navigation guard (check if user is logged before show the page)
router.beforeEach((to, from, next) => {
  const pageName: string = to.name + '';

  if (CredentialsUtils.enoughPermits(pageName)) {
    next();
  } else {
    if (CredentialsUtils.someoneIsLoggedIn()) {
      // If someone is logged in but doesn't have enough permits, it remains in the last page
      next(false);
      console.log('Non hai i permessi necessari!');
    } else {
      next({name: 'user-access'});
    }
  }
});

const bus = new EventBus.Bus();

new Vue({
  bus,
  router,
  render: (h) => h(App),
}).$mount('#app');


const socket = socketIO('http://localhost:8081/');
socket.on('connect', (data: any) => {
  console.log('Connected to socket');
  const callback = (message: any) => {
    console.log('Received message form server:');
    console.log(JSON.stringify(message, null, 2));
  };

  /*
    Two notifications kind: role-specific and user-specific

    Role-specific notifications must be observed with one of the following prefixes:
      "all", "none", "admin", "trainer", "user"
    The sintax is: {{prefix}}-{{event}}
    Currently enabled notifications:
      admin-newTrainer
      admin-newPlan

    Role-specific notifications must be observed using the user id as prefix
    The sintax is: {{userId}}-{{event}}
    Currently enabled notifications:
      Trainer:
        {{userId}}-newPlan
        {{userId}}-trainerAccepted
        {{userId}}-trainerRefused
        {{userId}}-planAccepted
        {{userId}}-planRefused
      User:
        {{userId}}-planCreated
  */

  // ADMIN

  socket.on('admin-newTrainer', callback);

  socket.on('admin-newPlan', callback);

  // TRAINER

  socket.on('5da1dd2805894c43acb9c479-newPlan', callback);

  socket.on('5da1dd2805894c43acb9c479-trainerAccepted', callback);

  socket.on('5da1dd2805894c43acb9c479-trainerRefused', callback);

  socket.on('5da1dd2805894c43acb9c479-planAccepted', callback);

  socket.on('5da1dd2805894c43acb9c479-planRefused', callback);

  // USER

  socket.on('5da1dd2805894c43acb9c479-planCreated', callback);

});
