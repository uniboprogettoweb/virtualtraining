import { Document, Schema, Model, model, Types } from 'mongoose';
import { Trainer } from '@/models/Trainer';
import { Request, Response } from 'express';
import { Muscle } from '@/models/Muscle';

export default abstract class MuscleController {

    public static getMuscles() {
		return (req: Request, res: Response) => {
			Muscle.find((err: any, muscles: Muscle[]) => {
                if (err) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Muscles not found',
                    });
                    return;
                }

                res.status(200).send(muscles);
            });
		};
    }

    public static createMuscle() {
		return (req: Request, res: Response) => {
            // console.log(req.body);
            if (!req.body || !req.body.name) {
                res.status(404).send({
                    description: 'Must specify a name',
                });
                return;
            }

            Muscle.find().sort({muscleId: -1}).limit(1).exec((err: any, muscles: Muscle[]) => {
                if (err) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Error while creating a new muscle',
                    });
                    return;
                }

                let newMuscleId = 1;
                if (muscles.length !== 0) {
                    newMuscleId = muscles[0].muscleId + 1;
                }

                const newMuscle = new Muscle({
                    muscleId: newMuscleId,
                    name: req.body.name,
                });
                newMuscle.save((createErr: any, createdMuscle: Muscle) => {
                    if (createErr) {
                        // console.log(createErr);
                        res.status(404).send({
                            description: 'Error while creating a new muscle',
                        });
                        return;
                    }

                    res.status(201).send(createdMuscle);
                });
            });
		};
    }

    public static updateMuscle() {
		return (req: Request, res: Response) => {
            // console.log(req.body);
            if (!req.body || !req.body.name || !req.params.muscleId) {
                res.status(404).send({
                    description: 'Must specify a name and an muscleId in path params',
                });
                return;
            }

            const muscleId = req.params.muscleId;
            const name = req.body.name;

            Muscle.findOneAndUpdate({
                muscleId,
            }, {
                name,
            }, {
                upsert: false,
            }, (err, oldMuscle) => {
                if (err || !oldMuscle) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Error while updating the muscle',
                    });
                    return;
                }

                res.status(200).send({
                    _id: oldMuscle._id,
                    muscleId: oldMuscle.muscleId,
                    name,
                });
            });
		};
    }

    public static deleteMuscle() {
		return (req: Request, res: Response) => {
            // console.log(req.body);
            if (!req.params.muscleId) {
                res.status(404).send({
                    description: 'Must specify an muscleId in path params',
                });
                return;
            }

            const muscleId = req.params.muscleId;

            Muscle.findOneAndDelete({
                muscleId,
            }, (err, oldMuscle) => {
                if (err || !oldMuscle) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Error while deleting the muscle',
                    });
                    return;
                }

                res.status(200).send({description: 'Muscle deleted successfully'});
            });
		};
    }

}
