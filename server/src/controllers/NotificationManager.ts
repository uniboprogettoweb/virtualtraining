import { Document, Schema, Model, model, Types } from 'mongoose';
import { Request, Response, NextFunction } from 'express';
import UserController from './UserController';
import { User } from '@/models/User';
import { Notification } from '@/models/Notification';
import NotificationController from './NotificationController';
import NotificationFactory from './NotificationFactory';

export default abstract class NotificationManager {

    // General user-type related notifications

    public static sendAllNotification(event: string, notification: Notification | any) {
        NotificationController.sendNotification('all-' + event, notification);
    }

    public static sendAdminNotification(event: string, notification: Notification | any) {
        NotificationController.sendNotification('admin-' + event, notification);
    }

    public static sendTrainerNotification(event: string, notification: Notification | any) {
        NotificationController.sendNotification('trainer-' + event, notification);
    }

    public static sendUserNotification(event: string, notification: Notification | any) {
        NotificationController.sendNotification('user-' + event, notification);
    }

    public static sendSpecificUserNotification(userId: string, event: string, notification: Notification | any) {
        NotificationController.sendNotification(userId + '-' + event, notification);
    }

    // Admin related notifications

    public static sendTrainerRegistrationNotification(trainerId: Types.ObjectId): Notification {
        const notification = NotificationFactory.createNewTrainerRegistrationNotification(trainerId);
        this.sendAdminNotification('newTrainer', notification);
        return notification;
    }

    // tslint:disable: max-line-length
    public static sendPlanApprovalRequestNotification(planId: Types.ObjectId): Notification {
        const notification = NotificationFactory.createPlanApprovalRequestNotification(planId);
        this.sendAdminNotification('newPlan', notification);
        return notification;
    }

    // Trainer related notifications

    // tslint:disable: max-line-length
    public static sendNewPlanRequestNotification(trainerId: Types.ObjectId, userId: Types.ObjectId, planId: Types.ObjectId): Notification {
        const notification = NotificationFactory.createNewPlanRequestNotification(trainerId, userId, planId);
        this.sendSpecificUserNotification(trainerId.toHexString(), 'newPlan', notification);
        return notification;
    }

    // tslint:disable: max-line-length
    public static sendTrainerAcceptedNotification(trainerId: Types.ObjectId): Notification {
        const notification = NotificationFactory.createTrainerAcceptedNotification(trainerId);
        this.sendSpecificUserNotification(trainerId.toHexString(), 'trainerAccepted', notification);
        return notification;
    }

    // tslint:disable: max-line-length
    public static sendTrainerRefusedNotification(trainerId: Types.ObjectId): Notification {
        const notification = NotificationFactory.createTrainerRefusedNotification(trainerId);
        this.sendSpecificUserNotification(trainerId.toHexString(), 'trainerRefused', notification);
        return notification;
    }

    // tslint:disable: max-line-length
    public static sendTrainerPlanAcceptedNotification(trainerId: Types.ObjectId, planId: Types.ObjectId): Notification {
        const notification = NotificationFactory.createTrainerPlanAcceptedNotification(trainerId, planId);
        this.sendSpecificUserNotification(trainerId.toHexString(), 'planAccepted', notification);
        return notification;
    }

    // tslint:disable: max-line-length
    public static sendTrainerPlanRefusedNotification(trainerId: Types.ObjectId, planId: Types.ObjectId): Notification {
        const notification = NotificationFactory.createTrainerPlanRefusedNotification(trainerId, planId);
        this.sendSpecificUserNotification(trainerId.toHexString(), 'planRefused', notification);
        return notification;
    }

    // User related notifications

    // tslint:disable: max-line-length
    public static sendPlanCreatedNotification(userId: Types.ObjectId, planId: Types.ObjectId): Notification {
        const notification = NotificationFactory.createPlanCreatedNotification(userId, planId);
        this.sendSpecificUserNotification(userId.toHexString(), 'planCreated', notification);
        return notification;
    }

}
