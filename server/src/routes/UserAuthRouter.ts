import { Application } from 'express';
import UserAuthController from '@/controllers/UserAuthController';
import AuthMiddleware from '@/controllers/AuthMiddleware';
import UserController from '@/controllers/UserController';

export default abstract class UserAuthRouter {
    /* tslint:disable */
	public static setupRoutes(app: Application) {
		app.route('/api/login')
			.post(UserAuthController.loginUser());

		app.route('/api/login-trainer')
			.post(UserAuthController.loginTrainer());
			
		app.route('/api/login-admin')
			.post(UserAuthController.loginAdmin());	

		app.route('/api/logout')
			.post(UserAuthController.validateUser(), UserAuthController.logoutUser());

		app.route('/api/register')
			.post(UserAuthController.createUser());

      	app.route('/api/register-trainer')
			.post(UserAuthController.createTrainer());

		app.route('/api/register-admin')
      		.post(UserAuthController.createAdmin());	  

		app.route('/api/user-is-logged')
			.get(UserAuthController.validateUser(), (req, res, next) => res.status(200).json({description: 'User is logged!'}));
	}
}
