import Api from '@/services/Api';

export interface Card {
  exercise: number;
  amount: number;
}

export interface Schedule {
  cards: Card[];
}

export interface Plan {
  planId: string;
  schedules: Schedule[];
}

export interface Info {}

export interface InfoTags extends Info {
  trainerTags: number[];
  muscles: number[];
}

export interface InfoPsw extends Info {
  oldPassword: string;
  password: string;
}

export interface InfoImage extends Info {
  profileImage: string;
}


export default {

  getTrainers() {
    return Api.withoutAuthorization().get('/api/trainers');
  },

  getTrainerInfo() {
    return Api.withTrainerAuthorization().get('/api/trainers/info');
  },

  getPlans() {
    return Api.withTrainerAuthorization().get('/api/trainers/plans');
  },

  getExercises() {
    return Api.withoutAuthorization().get('/api/exercises');
  },

  getMuscles() {
    return Api.withoutAuthorization().get('/api/muscles');
  },

  getUsers() {
    return Api.withoutAuthorization().get('/api/users');
  },

  postPlan(plan: Plan) {
    return Api.withTrainerAuthorization().post('/api/trainers/create-plan', plan);
  },

  postInfo(info: Info) {
    return Api.withTrainerAuthorization().post('/api/trainers/info', info);
  },
}
