import { Document, Schema, Model, model } from 'mongoose';
import { Muscle } from './Muscle';

export interface Exercise extends Document {
    exerciseId: number;
    name: string;
    description: string;
    isTimed: boolean;
    muscles: [Muscle];
    isMapNeeded: boolean;
    isWeightNeeded: boolean;
    exerciseImage: string;
}

const ExerciseSchema: Schema = new Schema({
    exerciseId: { type: Number, required: true, unique: true },
    name: { type: String, required: true },
    description: { type: String , required: true },
    isTimed: { type: Boolean, default: false },
    muscles: { type: Array, required: true, default: []},
    isMapNeeded: { type: Boolean, default: false },
    isWeightNeeded: { type: Boolean, default: false },
    exerciseImage: { type: String },
});

// Export the Exercise model and return the Exercise interface
export const Exercise: Model<Exercise> = model<Exercise>('Exercise', ExerciseSchema);
