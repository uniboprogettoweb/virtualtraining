import { Document, Schema, Model, model } from 'mongoose';

export interface TrainerTag extends Document {
    trainerTagId: number;
    name: string;
    description: string;
}

const TrainerTagSchema: Schema = new Schema({
    trainerTagId: { type: Number, required: true, unique: true },
    name: { type: String, required: true },
    description: { type: String },
});

// Export the TrainerTag model and return the TrainerTag interface
export const TrainerTag: Model<TrainerTag> = model<TrainerTag>('TrainerTag', TrainerTagSchema);
