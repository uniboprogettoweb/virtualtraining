"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CrudUtils {
    static getFilterOptions(fieldsName) {
        let filterOptions = [];
        fieldsName.forEach((name) => {
            filterOptions.push({
                value: name,
                text: 'Filter by ' + name + ':',
            });
        });
        return filterOptions;
    }
    static compareStringAsc(stringA, stringB) {
        var a = stringA.toLowerCase(), b = stringB.toLowerCase();
        if (a < b) //sort string ascending
            return -1;
        if (a > b)
            return 1;
        return 0; //default return value (no sorting)
    }
    static compareBooleanAsc(boolA, boolB) {
        return CrudUtils.compareNumberAsc(boolA ? 1 : 0, boolB ? 1 : 0);
    }
    static compareNumberAsc(numA, numB) {
        return numA - numB;
    }
    static compareNumberDesc(numA, numB) {
        return numB - numA;
    }
    static compareDateAsc(dateA, dateB) {
        if (dateA < dateB) //sort date ascending
            return -1;
        if (dateA > dateB)
            return 1;
        return 0;
    }
    static searchByName(collection, searchKey) {
        let filtered = [];
        const searchUpper = searchKey.toUpperCase();
        collection.forEach(elem => {
            const elemToUpp = elem.name.toUpperCase();
            if (elemToUpp.includes(searchUpper)) {
                filtered.push(elem);
            }
        });
        return filtered;
    }
    static searchByFirstName(collection, searchKey) {
        let filtered = [];
        const searchUpper = searchKey.toUpperCase();
        collection.forEach(elem => {
            const elemToUpp = elem.firstName.toUpperCase();
            if (elemToUpp.includes(searchUpper)) {
                filtered.push(elem);
            }
        });
        return filtered;
    }
    static searchByLastName(collection, searchKey) {
        let filtered = [];
        const searchUpper = searchKey.toUpperCase();
        collection.forEach(elem => {
            const elemToUpp = elem.lastName.toUpperCase();
            if (elemToUpp.includes(searchUpper)) {
                filtered.push(elem);
            }
        });
        return filtered;
    }
    static searchByAddress(collection, searchKey) {
        let filtered = [];
        const searchUpper = searchKey.toUpperCase();
        collection.forEach(elem => {
            const elemToUpp = elem.address.toUpperCase();
            if (elemToUpp.includes(searchUpper)) {
                filtered.push(elem);
            }
        });
        return filtered;
    }
    static searchByLevel(collection, searchKey) {
        let filtered = [];
        collection.forEach(elem => {
            if (elem.level == undefined)
                elem.level = 0;
            if (elem.level.toString().includes(searchKey))
                filtered.push(elem);
        });
        return filtered;
    }
    static searchByEmail(collection, searchKey) {
        let filtered = [];
        const searchUpper = searchKey.toUpperCase();
        collection.forEach(elem => {
            const elemToUpp = elem.email.toUpperCase();
            if (elemToUpp.includes(searchUpper)) {
                filtered.push(elem);
            }
        });
        return filtered;
    }
    static checkTextMaxLength(text, maxLength) {
        if (text == undefined)
            return '';
        return text.length > maxLength ?
            text.substring(0, maxLength) + '...' :
            text;
    }
}
exports.default = CrudUtils;
//# sourceMappingURL=CrudUtils.js.map