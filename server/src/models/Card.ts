import { Document, Schema, Model, model } from 'mongoose';
import { Exercise } from './Exercise';

export interface Card extends Document {
    exercise: Exercise;
    amount: number;
}

const CardSchema: Schema = new Schema({
    exercise: { type: Schema.Types.ObjectId, ref: Exercise, required: true},
    amount: { type: Number, required: true, min: 1},
});

// Export the Card model and return the Card interface
// tslint:disable-next-line: max-line-length
export const Card: Model<Card> = model<Card>('Card', CardSchema);
