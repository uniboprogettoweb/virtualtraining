import Muscle from './Muscle';

export default interface Coach {
  _id: string;
  accepted: boolean;
  address: string;
  birthdate: string;
  email: string;  ​
  firstName: string;
  lastName: string;
  profileImage: string;
  muscles: Muscle[];
  pending: boolean;
  rating: number;
  trainerTags: number[];
}
