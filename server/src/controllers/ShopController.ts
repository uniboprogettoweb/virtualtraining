import { Request, Response } from 'express';

import products from './json/TestProducts.json';
import products2 from './json/TestProductsForCat.json';
import categories from './json/TestCategories.json';
import userCart from './json/TestUserCartProducts.json';
import { Cart } from '@/models/Cart.js';
import { Types } from 'mongoose';
import { Product } from '@/models/Product.js';
import { CartProduct } from '@/models/CartProduct.js';
import ProductController from './ProductController.js';

export default abstract class ShopController {

  public static CART_PRODUCTS_LOOKUP = {
    $lookup: {
      from: 'products',
      localField: 'products.productId',
      foreignField: '_id',
      as: 'productsDetails',
    },
  };

  public static parseCartInfo(cartInfo: any): any {
    const cartInfoProducts: [] = cartInfo.products;
    const cartInfoProductsDetails: [] = cartInfo.productsDetails;
    const parsedProducts: any[] = [];

    cartInfoProducts.forEach((cip: any) => {
      cartInfoProductsDetails.forEach((cipd: any) => {
        // Necessary casting
        const productIdAsString = new Types.ObjectId(cip.productId).toHexString();
        const productDetailsIdAsString = new Types.ObjectId(cipd._id).toHexString();
        if (productIdAsString === productDetailsIdAsString) {
          // Manual join
          parsedProducts.push({
            _id: cip._id,
            product: cipd,
            quantity: cip.quantity,
          });
        }
      });
    });

    const cart = {
      _id: cartInfo._id,
      userId: cartInfo.userId,
      price: cartInfo.price,
      discountedPrice: cartInfo.discountedPrice,
      products: parsedProducts,
    };

    return cart;
  }

  public static getUserCart() {
    return (req: Request, res: Response) => {
      if (!req.user) {
        // console.log('User info not passed to getUserCart()');
        res.status(404).send({
          description: 'User not found, the requested _id seems incorrect',
        });
        return;
      }

      // Retrieving user id
      const userId = req.user._id;
      // Retrieve the user cart
      Cart.aggregate([
        {
          $match: {
            userId: new Types.ObjectId(userId),
          },
        },
        this.CART_PRODUCTS_LOOKUP,
      ]).exec((err, cartInfo: any[]) => {
        if (err) {
          // console.log(err);
          res.status(404).send({
            description: 'Cannot retrieve the user cart detailed products to an error',
          });
          return;
        }

        if (!cartInfo || cartInfo.length === 0) {
          // No cartInfo found, we need a new cart
          // console.log('No cart found, we create a new one');

          const newCart = new Cart({
            userId: new Types.ObjectId(userId),
            discountedPrice: 0.0,
          });

          // No cart found, we create a new one
          // console.log('No cart found, we create a new one');

          newCart.save((saveErr, savedCart) => {
            if (saveErr) {
              // console.log(saveErr);
              res.status(404).send({
                description: 'Cannot save the new user cart due to an error',
              });
              return;
            }

            // console.log('Created new cart:\n');
            // console.log(savedCart);
            res.status(201).send(savedCart);
          });

        } else {
          // We found the user cart
          const info: any = cartInfo[0];
          const parsedCartInfo = this.parseCartInfo(info);

          // console.log('Retrieved saved cart:\n');
          res.status(200).send(parsedCartInfo);
        }
      });

    };
  }

  public static postUpdateCartProduct() {
    return (req: Request, res: Response) => {
      if (!req.user) {
        // console.log('User info not passed to postUpdateCartProduct()');
        res.status(404).send({
          description: 'User not found, the requested _id seems incorrect',
        });
        return;
      }

      if (!req.body || !req.body.productId || req.body.quantity == null || req.body.quantity === undefined) {
        res.status(500).json({
          description: 'productId or quantity are missing',
        });
        return;
      }

      // Retrieving user id
      const userId = req.user._id;
      const productId = req.body.productId;
      const quantity = req.body.quantity;

      Cart.aggregate([
        {
          $match: {
            userId: new Types.ObjectId(userId),
          },
        },
        this.CART_PRODUCTS_LOOKUP,
      ]).exec((err, carts: any[]) => {
        if (err) {
          // console.log(err);
          res.status(404).send({
            description: 'Cannot retrieve the user cart due to an error',
          });
          return;
        }

        if (!carts || carts.length === 0) {
          // No cart found
          // console.log('No cart found');
          res.status(404).send({
            description: 'No cart found, the product could not be updated',
          });
          return;
        }

        // We found the user cart, we must update the cart
        const cartInfo = this.parseCartInfo(carts[0]);

        Product.aggregate([
          {
            $match: {
              productId,
            },
          },
          ProductController.PRODUCTS_EXERCISES_LOOKUP,
          ProductController.PRODUCTS_CATEGORY_LOOKUP,
        ]).exec((productErr: any, retrievedProducts: Product[]) => {
          if (productErr) {
            // console.log(err);
            res.status(404).send({
              description: 'Error while retrieving product',
            });
            return;
          }
          if (!retrievedProducts || retrievedProducts.length === 0) {
            // console.log(err);
            res.status(404).send({
              description: 'Product not found',
            });
            return;
          }

          const retrievedProduct = retrievedProducts[0];

          // Updating cart products inside cart itself
          this.updateProductInCart(cartInfo, retrievedProduct, quantity);

          // Updating cart price, based on the current prices
          this.updateCartPrice(cartInfo);

          const copyUpdateProducts = cartInfo.products;
          // Update cart products
          const finalCartProducts = cartInfo.products.map((currentProduct: any) => {
            return {
              productId: currentProduct.product._id,
              quantity: currentProduct.quantity,
            };
          });
          cartInfo.products = finalCartProducts;

          // We must save the cart
          Cart.findByIdAndUpdate(cartInfo._id, cartInfo, (updateErr, updatedCart) => {
            if (updateErr) {
              // console.log(updateErr);
              res.status(404).send({
                description: 'Cannot update the user cart due to an error',
              });
              return;
            }

            // Resetting useful products info
            cartInfo.products = copyUpdateProducts;
            res.status(200).send(cartInfo);
          });

        });

      });
    };
  }

  public static deleteUserCart() {
    return (req: Request, res: Response) => {
      if (!req.user) {
        // console.log('User info not passed to deleteUserCart()');
        res.status(404).send({
          description: 'User not found, the requested _id seems incorrect',
        });
        return;
      }

      // Retrieving user id
      const userId = req.user._id;

      Cart.findOneAndDelete({
        userId: new Types.ObjectId(userId),
      }, (err, deletedCart) => {
        if (err) {
          // console.log(err);
          res.status(404).send({
            description: 'Cannot delete the user cart due to an error',
          });
          return;
        }

        return res.status(200).json({ msg: 'Cart deleted successfully' });
      });

    };
  }

  private static updateProductInCart(cart: any, productToBeUpdated: Product, quantity: number) {
    let cartProducts: any[] = cart.products;
    if (cartProducts) {
      const cartProductsIds = cartProducts.map((p: any) => p.product.productId);
      const productToBeUpdatedId = productToBeUpdated.productId;
      if (cartProductsIds.includes(productToBeUpdatedId)) {
        // The cart already contains the product
        if (quantity <= 0) {
          // console.log('DELETING PRODUCT');

          // The user wants to delete the product
          cartProducts = cartProducts.filter((p: any) => p.product.productId !== productToBeUpdatedId);
        } else {
          // console.log('UPDATING PRODUCT');

          // The user wants to update the product quantity
          const foundProduct = cartProducts.find((p: any) => p.product.productId === productToBeUpdatedId);

          if (foundProduct) {
            foundProduct.quantity = quantity;
          }
        }
      } else {
        if (quantity > 0) {
          // The cart does not contain the product, which is now created
          // console.log('ADDING NEW PRODUCT');

          const newCartProduct = {
            product: productToBeUpdated,
            quantity,
          };

          cartProducts.push(newCartProduct);
        }
      }
    }
    cart.products = cartProducts;
  }

  private static updateCartPrice(cart: any) {
    if (cart) {
      let currentPrice = 0.0;
      let currentDiscountedPrice = 0.0;
      cart.products.forEach((pc: any) => {
        currentPrice += (pc.product.price * pc.quantity);
        currentDiscountedPrice += (pc.product.discountedPrice * pc.quantity);
      });

      cart.price = currentPrice;
      cart.discountedPrice = currentDiscountedPrice;
    }
  }
}
