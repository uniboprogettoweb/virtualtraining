import { Document, Schema, Model, model } from 'mongoose';

export interface Muscle extends Document {
    muscleId: number;
    name: string;
}

const MuscleSchema: Schema = new Schema({
    muscleId: { type: Number, unique: true, required: true },
    name: { type: String, required: true },
});

// Export the Muscle model and return the Muscle interface
export const Muscle: Model<Muscle> = model<Muscle>('Muscle', MuscleSchema);
