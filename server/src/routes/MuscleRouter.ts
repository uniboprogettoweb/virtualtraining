import { Application } from 'express';
import MuscleController from '@/controllers/MuscleController';

export default abstract class MuscleRouter {
	public static setupRoutes(app: Application) {
        app.route('/api/muscles')
            .get(MuscleController.getMuscles())
            .post(MuscleController.createMuscle());

        app.route('/api/muscles/:muscleId')
            .put(MuscleController.updateMuscle())
            .delete(MuscleController.deleteMuscle());
    }
}
