import { Document, Schema, Model, model } from 'mongoose';
import { Achievement } from './Achievement';
import { Exercise } from './Exercise';
import { Card } from './Card';

export interface User extends BaseUser {
	firstName: string;
	lastName: string;
	sex: number;
	birthdate: Date;
	address: string;
	profileImage: string;
	level: number;
	experience: number;
	achievements: Achievement[];
	exercises: Card[];
}

export interface BaseUser extends Document {
	email: string;
	password: string;
	isTrainer: boolean;
	isAdmin: boolean;
	salt: string;
	createdAt: Date;
	updatedAt: Date;
	tokens: string[];
}

const UserSchema: Schema = new Schema({
	email: { type: String, required: true, unique: true },
	password: { type: String, required: true },
	firstName: { type: String, required: true },
	lastName: { type: String, required: true },
	sex: { type: Number, required: true, default: 0 },
	birthdate: { type: Date, required: true },
	address: { type: String, required: true },
	profileImage: { type: String },
	level: { type: Number, default: 1 },
	experience: { type: Number, default: 0 },
	achievements: {type: Array , required: true, default: []},
	exercises: {type: Array , required: true, default: []},
	isTrainer: { type: Boolean, required: true, default: false },
	isAdmin: { type: Boolean, required: true, default: false },
	salt: { type: String, required: true},
	createdAt: Date,
	updatedAt: Date,
	tokens: {type: Array , required: true, default: []},
});

// Update time related fields
UserSchema.pre<User>('validate', function(next) {
	if (this) {
		const now = new Date();
		if (!this.createdAt) {
			this.createdAt = now;
		}
		this.updatedAt = now;
	}
	next();
});

// Export the User model and return the User interface
export const User: Model<User> = model<User>('User', UserSchema);
