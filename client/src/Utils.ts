import Vue from 'vue';
import {permissionLevelUser, permissionLevelCoach, permissionLevelAdmin} from './router';

class CredentialsUtilsClass extends Vue {
    // Cookies names used to understand how is logged in
    public readonly user: string = 'userIsLogged';
    public readonly coach: string = 'coachIsLogged';
    public readonly admin: string = 'adminIsLogged';
    private readonly nooneLoggedIn: string = '___NO____';

    public userIsLogged(): boolean {
        return this.$cookies.isKey(this.user);
    }

    public coachIsLogged(): boolean {
        return Vue.cookies.isKey(this.coach);
    }

    public adminIsLogged(): boolean {
        return Vue.cookies.isKey(this.admin);
    }

    public someoneIsLoggedIn() {
        return !(this.whoIsLoggedIn() === this.nooneLoggedIn);
    }

    public enoughPermits(pageName: string): boolean {
        if (this.pageRequirePermissions(pageName)) {
            switch (this.whoIsLoggedIn()) {
                case this.user:
                    return permissionLevelUser.includes(pageName);
                    break;
                case this.coach:
                    return permissionLevelCoach.includes(pageName);
                    break;
                case this.admin:
                    return permissionLevelAdmin.includes(pageName);
                    break;
                default:
                    return false;
            }
        } else {
            return true;
        }
    }

    private whoIsLoggedIn(): string {
        if (this.userIsLogged()) { return this.user; }
        if (this.coachIsLogged()) { return this.coach; }
        if (this.adminIsLogged()) { return this.admin; }
        return this.nooneLoggedIn;
    }

    private pageRequirePermissions(pageName: string): boolean {
        return permissionLevelUser.includes(pageName) || permissionLevelCoach.includes(pageName) || permissionLevelAdmin.includes(pageName);
    }
}

export const CredentialsUtils = new CredentialsUtilsClass();
