import { Document, Schema, Model, model, Types } from 'mongoose';
import bcrypt from 'bcryptjs';
import { Request, Response, NextFunction } from 'express';
import { User, BaseUser } from '@/models/User';
import jwt from 'jsonwebtoken';
import { Trainer } from '@/models/Trainer';
import UserController from './UserController';
import TrainersController from './TrainersController';
import { Admin } from '@/models/Admin';

export default abstract class AuthMiddleware {

	public static AUTH_KEY: string = 'r512GlLlcy';

	public static generateJWTToken(baseUser: BaseUser): string {
        const token = jwt.sign({
            _id: baseUser._id,
            isAdmin: baseUser.isAdmin,
            isTrainer: baseUser.isTrainer,
        }, this.AUTH_KEY, {expiresIn: '10h'});
        // console.log('Created token: ' + token);
        return token;
	}

    /* tslint:disable */
	public static validateUser() {
		return (req: Request, res: Response, next: NextFunction) => {
            // Retrieve token
            const token = req.headers.authorization;
            //console.log(token);
		    if (!token) {
				//console.log('Auth token was empty');
				return res.status(401).send('Access denied, the user must login first');
			}
		    try {

                const decodedUser: any = jwt.verify(token, this.AUTH_KEY, {
                    ignoreExpiration: true,
                });
                
                //console.log('Decoded user');
                //console.log(decodedUser);

                const matchQuery = {
                    $match: {
                        _id: new Types.ObjectId(decodedUser._id),
                        tokens: token, 
                    }
                };

                const isAdmin = decodedUser.isAdmin;
                if (isAdmin) {
                    Admin.aggregate([
                        matchQuery,
                    ]).exec((err, existingAdmins) => {
                        if (err) {
                            //console.log(err);
                            res.status(401).send('Invalid token');
                            return; 
                        }
                        if (!existingAdmins || existingAdmins.length == 0) {
                            //console.log('Admin not found while validation');
                            res.status(401).send('Invalid token');
                            return; 
                        }
                        // The admin exists and is valid, we save the reference for the following request
                        req.user = existingAdmins[0];
                        next();
                    });
                } else {
                    const isTrainer = decodedUser.isTrainer;
                    if (isTrainer) {
                        Trainer.aggregate([
                            matchQuery,
                            TrainersController.TRAINER_MUSCLES_LOOKUP,
                            TrainersController.TRAINER_TRAINERTAGS_LOOKUP,
                        ]).exec((err, existingTrainers) => {
                            if (err) {
                                //console.log(err);
                                res.status(401).send('Invalid token');
                                return; 
                            }
                            if (!existingTrainers || existingTrainers.length === 0) {
                                //console.log('Trainer not found while validation');
                                res.status(401).send('Invalid token');
                                return; 
                            }
                            // The user exists and is valid, we save the reference for the following request
                            //console.log(existingTrainers[0]);
                            req.user = existingTrainers[0];
                            next();
                        });
                    } else {
                        User.aggregate([
                            matchQuery,
                            UserController.USER_ACHIEVEMENT_LOOKUP,
                        ]).exec((err, existingUsers) => {
                            if (err) {
                                //console.log(err);
                                res.status(401).send('Invalid token');
                                return; 
                            }
                            if (!existingUsers || existingUsers.length == 0) {
                                //console.log('User not found while validation');
                                res.status(401).send('Invalid token');
                                return; 
                            }
                            // The user exists and is valid, we save the reference for the following request
                            req.user = existingUsers[0];
                            next();
                        });
                    }
                }      
			} catch (error) {
				//console.log(error);
				res.status(400).send('Invalid token');
			}
		};
	}
}
