import { Application } from 'express';
import CategoryController from '@/controllers/CategoryController';

export default abstract class CategoryRouter {
	public static setupRoutes(app: Application) {
        app.route('/api/shop/categories')
            .get(CategoryController.getCategories());

        app.route('/api/shop/categories')
            .put(CategoryController.createCategory());
    }
}
