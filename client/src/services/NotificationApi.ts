import Api from '@/services/Api';
import {CredentialsUtils} from '../Utils';

interface Id {
  notifications: string[];
}

export default {
  getNotification() {
    if (CredentialsUtils.userIsLogged()) {
      return Api.withAuthorization().get('/api/user-notifications');
    } else if (CredentialsUtils.coachIsLogged()) {
      return Api.withTrainerAuthorization().get('/api/user-notifications');
    } else {
      return Api.withAdminAuthorization().get('/api/user-notifications');
    }
  },
  postReadNotification(notifications: Id) {
    if (CredentialsUtils.userIsLogged()) {
      return Api.withAuthorization().post('/api/user-notifications', notifications);
    } else if (CredentialsUtils.coachIsLogged()) {
      return Api.withTrainerAuthorization().post('/api/user-notifications', notifications);
    } else {
      return Api.withAdminAuthorization().post('/api/user-notifications', notifications);
    }

  },
  deleteNotification(notifications: Id) {
    if (CredentialsUtils.userIsLogged()) {
      return Api.withAuthorization().delete('/api/user-notifications',{data:notifications});
    } else if (CredentialsUtils.coachIsLogged()) {
      return Api.withTrainerAuthorization().delete('/api/user-notifications',{data:notifications});
    } else {
      return Api.withAdminAuthorization().delete('/api/user-notifications',{data:notifications});
    }

  },
};
