import { Document, Schema, Model, model, Types } from 'mongoose';
import { Request, Response, NextFunction } from 'express';
import UserController from './UserController';
import { User } from '@/models/User';
import { Notification, UserType, NotificationType } from '@/models/Notification';
import NotificationRouter from '@/routes/NotificationRouter';

export default abstract class NotificationFactory {

    // Admins

    public static createNewTrainerRegistrationNotification(trainerId: Types.ObjectId): Notification {
        return new Notification({
            notificationType: NotificationType.NEW_TRAINER_REGISTRATION,
            userType: UserType.ADMIN,
            title: 'A new Trainer has signed up',
            description: 'A new Trainer has created an account and is waiting to be enabled',
            date: new Date(),
            userId: trainerId,
            readBy: [],
            deletedBy: [],
        });
    }

    // tslint:disable: max-line-length
    public static createPlanApprovalRequestNotification(planId: Types.ObjectId): Notification {
        return new Notification({
            notificationType: NotificationType.PLAN_APPROVAL_REQUEST,
            userType: UserType.ADMIN,
            title: 'New request for plan approval',
            description: 'A Trainer has created a plan and is waiting its approval',
            date: new Date(),
            planId,
            readBy: [],
            deletedBy: [],
        });
    }

    // Trainer

    // tslint:disable: max-line-length
    public static createNewPlanRequestNotification(trainerId: Types.ObjectId, userId: Types.ObjectId, planId: Types.ObjectId): Notification {
        return new Notification({
            notificationType: NotificationType.NEW_PLAN_REQUEST,
            userType: UserType.NONE,
            receiverId: trainerId,
            title: 'A user has requested a new plan',
            description: 'A user has chosen you and has requested a training plan',
            date: new Date(),
            userId,
            planId,
            readBy: [],
            deletedBy: [],
        });
    }

    // tslint:disable-next-line: max-line-length
    public static createTrainerAcceptedNotification(trainerId: Types.ObjectId): Notification {
        return new Notification({
            notificationType: NotificationType.TRAINER_ACCEPTED,
            userType: UserType.NONE,
            receiverId: trainerId,
            title: 'Your profile has been accepted by an admin',
            description: 'You have been successfully registered as a Trainer. Your profile will be visible to users who may request a training plan according to your tags',
            date: new Date(),
            readBy: [],
            deletedBy: [],
        });
    }

    // tslint:disable-next-line: max-line-length
    public static createTrainerRefusedNotification(trainerId: Types.ObjectId): Notification {
        return new Notification({
            notificationType: NotificationType.TRAINER_REFUSED,
            userType: UserType.NONE,
            receiverId: trainerId,
            title: 'Your profile has been rejected by an admin',
            description: 'Your sign up request as a Trainer has been rejected. Please check your info and try again',
            date: new Date(),
            readBy: [],
            deletedBy: [],
        });
    }

    // tslint:disable-next-line: max-line-length
    public static createTrainerPlanAcceptedNotification(trainerId: Types.ObjectId, planId: Types.ObjectId): Notification {
        return new Notification({
            notificationType: NotificationType.TRAINER_PLAN_ACCEPTED,
            userType: UserType.NONE,
            receiverId: trainerId,
            title: 'The plan you have created has been accepted by an admin',
            description: 'An admin has checked your plan proposal and has approved it',
            date: new Date(),
            planId,
            readBy: [],
            deletedBy: [],
        });
    }

    // tslint:disable-next-line: max-line-length
    public static createTrainerPlanRefusedNotification(trainerId: Types.ObjectId, planId: Types.ObjectId): Notification {
        return new Notification({
            notificationType: NotificationType.TRAINER_PLAN_REFUSED,
            userType: UserType.NONE,
            receiverId: trainerId,
            title: 'The plan you have created has been rejected by an admin',
            description: 'An admin has checked your plan proposal and has refused it',
            date: new Date(),
            planId,
            readBy: [],
            deletedBy: [],
        });
    }

    // User

    public static createPlanCreatedNotification(userId: Types.ObjectId, planId: Types.ObjectId): Notification {
        return new Notification({
            notificationType: NotificationType.PLAN_CREATED,
            userType: UserType.NONE,
            receiverId: userId,
            title: 'A new training plan is available for you!',
            description: 'The Trainer you have chose has created a new training plan according to your needs',
            date: new Date(),
            planId,
            readBy: [],
            deletedBy: [],
        });
    }
}
