// Alias resolver, as seen in package.json _moduleAliases and tsconfing.json paths.
// It always refer to transpiled code in /dist
import 'module-alias/register';
import express from 'express';
import passport from 'passport';
import socketIO from 'socket.io';
/*Note Session data is not saved in the cookie itself, just the session ID. Session data is stored server-side.*/

// const session = require('express-session');
import cookieSession from 'cookie-session';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';
import mongoose from 'mongoose';
import TrainersRouter from '@/routes/TrainersRouter';
import UserAuthRouter from '@/routes/UserAuthRouter';
import MuscleRouter from './routes/MuscleRouter';
import ShopRoutes from './routes/ShopRoutes';
import UserRouter from './routes/UserRouter';
import ExerciseRouter from './routes/ExerciseRouter';
import AchievementRouter from './routes/AchievementRouter';
import CoachRequestsRouter from '@/routes/CoachRequestsRouter';
import CoachInfoRouter from '@/routes/CoachInfoRouter';
import AdminRouter from '@/routes/AdminRouter';
import CategoryRouter from './routes/CategoryRouter';
import ProductRouter from './routes/ProductRouter';
import LeaderboardRouter from './routes/LeaderboardRouter';
import NotificationRouter from './routes/NotificationRouter';
import NotificationController from './controllers/NotificationController';

const app = express();
app.use(morgan('combined'));

// Cors config:
//  origin -> url of the origin of the admitted requests
//  credential: true -> Configures the Access-Control-Allow-Credentials CORS header. [Required to use cookies]
app.use(cors({
	origin: 'http://localhost:8080',
	credentials: true,
	}));

// Body parser rules
app.use(bodyParser.urlencoded({limit: '50mb'}));
app.use(bodyParser.json({limit: '50mb'}));

// Middleware cookie session
const cookieSessionOptions: CookieSessionInterfaces.CookieSessionOptions = {
	name: 'virtualtraining',
	keys: ['secretAndSuperRandomKey'],
	maxAge: 24 * 60 * 60 * 1000,

	// cookie: {secure: true}
};
app.use(cookieSession(cookieSessionOptions));

// Passport middleware
// Important: set them after express session to avoid problems
app.use(passport.initialize());
app.use(passport.session());

// tslint:disable-next-line: max-line-length
const mongoUri = 'mongodb://marco:virtualtrainingpass0@virtualtrainingcluster-shard-00-00-cpvpt.mongodb.net:27017,virtualtrainingcluster-shard-00-01-cpvpt.mongodb.net:27017,virtualtrainingcluster-shard-00-02-cpvpt.mongodb.net:27017/virtualtraining?ssl=true&replicaSet=VirtualTrainingCluster-shard-0&authSource=admin&retryWrites=true&w=majority';
mongoose.connect(mongoUri, {useNewUrlParser: true }, (err) => {
	if (err) {
		console.log(err);
	} else {
		console.log('Connection established with MongoDB at ' + mongoUri);
	}
});

// Setup the routers
app.options('*', cors()); // include before other routes

UserAuthRouter.setupRoutes(app);
UserRouter.setupRoutes(app);
TrainersRouter.setupRoutes(app);
AdminRouter.setupRoutes(app);
MuscleRouter.setupRoutes(app);
ExerciseRouter.setupRoutes(app);
CategoryRouter.setupRoutes(app);
ProductRouter.setupRoutes(app);
ShopRoutes.setupRoutes(app);
AchievementRouter.setupRoutes(app);
LeaderboardRouter.setupRoutes(app);
CoachRequestsRouter.setupRoutes(app);
CoachInfoRouter.setupRoutes(app);

// Setup the server
const port = 8081;
const server = app.listen(port, () => {
	console.log('Server up on port ' + port);
});

NotificationRouter.setupRoutes(app, server);
