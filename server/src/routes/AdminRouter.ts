import { Application } from 'express';
import UserController from '@/controllers/UserController';
import UserAuthController from '@/controllers/UserAuthController';
import AdminController from '@/controllers/AdminController';

export default abstract class AdminRouter {
    /* tslint:disable */
	public static setupRoutes(app: Application) {

		app.route('/api/admin/pending-trainers')
			.get(AdminController.validateUser(), AdminController.getPendingTrainers());

        app.route('/api/admin/pending-plans')
			.get(AdminController.validateUser(), AdminController.getPendingPlans());

        app.route('/api/admin/validate-trainer')
            .post(AdminController.validateUser(), AdminController.postValidateTrainer());
        
        app.route('/api/admin/validate-plan')
			.post(AdminController.validateUser(), AdminController.postValidatePlan());    
	}
}
