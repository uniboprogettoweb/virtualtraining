import { Application } from 'express';
import UserController from '@/controllers/UserController';
import UserAuthController from '@/controllers/UserAuthController';
import NotificationController from '@/controllers/NotificationController';

export default abstract class UserRouter {
    /* tslint:disable */
	public static setupRoutes(app: Application) {

		app.route('/api/users')
			.get(UserController.getUsers())
			.post(UserAuthController.createUser());

		app.route('/api/users/info')
			.get(UserAuthController.validateUser(), UserController.getUserInfo());

		app.route('/api/users/notifications')
			.get(UserAuthController.validateUser(), NotificationController.getUserNotifications());

		app.route('/api/users/info')
			.post(UserAuthController.validateUser(), UserController.postUpdateUserInfo());

		app.route('/api/users/request-plan')
			.post(UserAuthController.validateUser(), UserController.postRequestPlan());

		app.route('/api/users/plans')
			.get(UserAuthController.validateUser(), UserController.getUserPlans());

		app.route('/api/users/rate-plan')
			.post(UserAuthController.validateUser(), UserController.postRatePlan());

		app.route('/api/users/complete-exercise')
			.post(UserAuthController.validateUser(), UserController.postCompleteExercise());

		app.route('/api/users/:id')
			.get(UserController.getUser())
			.put(UserController.updateUserInfo())
			.delete(UserController.deleteUser());

	}
}
