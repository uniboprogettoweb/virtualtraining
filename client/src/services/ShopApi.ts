import Api from '@/services/Api';


export default {
  // ___________________________GET_REQUESTS______________________
  getCategories() {
    return Api.withoutAuthorization().get('/api/shop/categories');
  },
  getAllProducts() {
    return Api.withoutAuthorization().get('/api/shop/products');
  },
  getProductsByCategory(catId: number) {
    const catArray = [];
    catArray.push(catId);
    return Api.withoutAuthorization().get('/api/shop/products', {
      params: {
        categories: JSON.stringify(catArray),
      },
    });
  },
};
