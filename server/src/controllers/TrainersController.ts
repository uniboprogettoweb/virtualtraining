import { Document, Schema, Model, model, Types } from 'mongoose';
import { Trainer } from '@/models/Trainer';
import { Request, Response, NextFunction } from 'express';
import { Plan } from '@/models/Plan';
import { Schedule } from '@/models/Schedule';
import { Card } from '@/models/Card';
import UserController from './UserController';
import { Exercise } from '@/models/Exercise';
import { Product } from '@/models/Product';
import ExerciseController from './ExerciseController';
import NotificationManager from './NotificationManager';
import { Notification } from '@/models/Notification';

export default abstract class TrainersController {

	public static TRAINER_MUSCLES_LOOKUP = {
		$lookup: {
			from: 'muscles',
			localField: 'muscles',
			foreignField: 'muscleId',
			as: 'muscles',
		},
	};

	public static TRAINER_TRAINERTAGS_LOOKUP = {
		$lookup: {
			from: 'trainertags',
			localField: 'trainerTags',
			foreignField: 'trainerTagId',
			as: 'trainerTags',
		},
	};

	public static TRAINER_SCHEDULES_CARDS_LOOKUP = {
		$lookup: {
			from: 'exercises',
			localField: 'schedules.cards._id',
			foreignField: '_id',
			as: 'schedules.cards.exercise',
		},
	};

	public static parseTrainer(trainer: Trainer) {
		const trainerInfo = {
			_id: trainer._id,
			email: trainer.email,
			firstName: trainer.firstName,
			lastName: trainer.lastName,
			birthdate: trainer.birthdate,
			address: trainer.address,
			trainerTags: trainer.trainerTags,
			muscles: trainer.muscles,
			rating: trainer.rating,
			profileImage: trainer.profileImage,
			description: trainer.description,
			accepted: trainer.accepted,
			pending: trainer.pending,
		};
		return trainerInfo;
	}

	public static getTrainer() {
		return (req: Request, res: Response) => {
			Trainer.aggregate([
				{ $match: { _id: new Types.ObjectId(req.params.id), accepted: true } },
				this.TRAINER_MUSCLES_LOOKUP,
				this.TRAINER_TRAINERTAGS_LOOKUP,
			]).exec((err: any, trainers: any[]) => {
				if (err) {
					// console.log(err);
					res.status(404).send({
						description: 'Trainer not found, the requested _id seems incorrect',
					});
				} else {
					if (trainers == null || trainers.length === 0) {
						res.status(404).send({
							description: 'Trainer not found',
						});
					} else {
						const parsedTrainer = this.parseTrainer(trainers[0]);
						res.status(200).json(parsedTrainer);
					}
				}
			});
		};
	}

	public static getTrainers() {
		return (req: Request, res: Response) => {
			const musclesIds = req.query.muscles ? JSON.parse(req.query.muscles) : null;
			const trainerTagsIds = req.query.trainerTags ? JSON.parse(req.query.trainerTags) : null;

			// console.log(musclesIds);
			// console.log(trainerTagsIds);

			const callback = (err: any, trainers: any[]) => {
				// console.log(trainers);
				if (err) {
					// console.log(err);
					res.status(404).send({
						description: 'Trainers not found',
					});
				} else {
					if (trainers == null) {
						res.status(404).send({
							description: 'Trainers not found',
						});
					} else {
						const parsedTrainers = trainers.map((t) => this.parseTrainer(t));
						res.status(200).json(parsedTrainers);
					}
				}
			};
			// Or clause
			const orArray = [];
			if (musclesIds) {
				orArray.push({
					muscles: { $in: musclesIds },
				});
			}
			if (trainerTagsIds) {
				orArray.push({
					trainerTags: { $in: trainerTagsIds },
				});
			}

			// Aggregate clause
			const aggregateQuery = [];
			if (orArray.length > 0) {
				aggregateQuery.push({
					$match: {
						$or: orArray,
					},
				});
			}
			aggregateQuery.push(this.TRAINER_MUSCLES_LOOKUP);
			aggregateQuery.push(this.TRAINER_TRAINERTAGS_LOOKUP);
			// Executing query
			Trainer.aggregate(aggregateQuery).exec(callback);
		};
	}

	public static getTrainerInfo() {
		return (req: Request, res: Response, next: NextFunction) => {
			if (!req.user) {
				// console.log('User info not passed to getTrainerInfo()');
				res.status(404).send({
					description: 'User not found, the requested _id seems incorrect',
				});
				return;
			}
			// console.log(req.user);
			// Retrieving user id
			const trainerId = req.user._id;
			Trainer.aggregate([
				{ $match: { _id: new Types.ObjectId(trainerId) } },
				this.TRAINER_MUSCLES_LOOKUP,
				this.TRAINER_TRAINERTAGS_LOOKUP,
			]).exec((err: any, trainers: any[]) => {
				if (err) {
					// console.log(err);
					res.status(404).send({
						description: 'Trainer not found, the requested _id seems incorrect',
					});
				} else {
					if (trainers == null || trainers.length === 0) {
						res.status(404).send({
							description: 'Trainer not found',
						});
					} else {
						const parsedTrainer = this.parseTrainer(trainers[0]);
						res.status(200).json(parsedTrainer);
					}
				}
			});
		};
	}

	public static getTrainerPlans() {
		return (req: Request, res: Response, next: NextFunction) => {
			if (!req.user) {
				// console.log('User info not passed to getTrainerPlans()');
				res.status(404).send({
					description: 'User not found, the requested _id seems incorrect',
				});
				return;
			}

			// Retrieving user id
			const userId = req.user._id;
			const matchQuery = {
				$match: {
					compiledBy: new Types.ObjectId(userId),
				},
			};

			// console.log(userId);
			// console.log(matchQuery);

			Plan.aggregate([
				matchQuery,
				TrainersController.TRAINER_TRAINERTAGS_LOOKUP,
				TrainersController.TRAINER_MUSCLES_LOOKUP,
			]).exec((err, plans: any[]) => {
				if (err) {
					// console.log(err);
					res.status(404).send({
						description: 'Error while retrieving user plans',
					});
					return;
				}

				res.status(200).send(plans);
			});
		};
	}

	public static postCreatePlan() {
		return (req: Request, res: Response, next: NextFunction) => {
			if (!req.user) {
				// console.log('User info not passed to postCreatePlan()');
				res.status(404).send({
					description: 'User not found, the requested _id seems incorrect',
				});
				return;
			}

			if (!req.body || !req.body.planId || !req.body.schedules) {
				// console.log('User info not passed to postCreatePlan()');
				res.status(404).send({
					description: 'planId and schedules fields must be inserted',
				});
				return;
			}

			const bodySchedules: Schedule[] = req.body.schedules;
			const requiredCards: any[] = [];
			const requiredExercises: any[] = [];
			const exercisesIds: number[] = [];

			// Finding productsIds
			bodySchedules.forEach((schedule: any) => {
				schedule.cards.forEach((card: any) => {
					const exerciseId = card.exercise;
					// Saving reference
					requiredCards.push({
						amount: card.amount,
						weight: card.weight,
						exerciseId: card.exercise,
					});
					// Saving exercise ids (if not present)
					if (!exercisesIds.includes(exerciseId)) {
						exercisesIds.push(exerciseId);
					}
				});
			});

			const exercisesPromises = exercisesIds.map((id) => {
				// Creating a promise for every exercise
				return Exercise
				.aggregate([
					{
						$match: { exerciseId: id },
					},
					ExerciseController.EXERCISE_MUSCLES_LOOKUP,
				])
				.exec();
			});
			// Joining the promises
			Promise.all(exercisesPromises).then((results) => {
				results.forEach((exercises) => {
					if (exercises != null && exercises.length !== 0) {
						// Retrieving the exercise
						const foundExercise = exercises[0];
						requiredExercises.push({
							exerciseId: foundExercise.exerciseId,
							exercise: foundExercise,
						});
					}
				});

				const newSchedules: Schedule[] = bodySchedules.map((schedule: any, index: number) => {
					const newCards = schedule.cards
						.map((card: any) => {
							const reqExercise = requiredExercises.find((reqEx) => reqEx.exerciseId === card.exercise).exercise;
							return {
								amount: card.amount,
								weight: card.weight,
								exerciseId: card.exerciseId,
								exercise: reqExercise,
							};
						});

					return new Schedule({
						cards: newCards,
					});
				});

				// Retrieving user id
				const userId = req.user._id;
				const matchQuery = {
					$match: {
						compiledBy: new Types.ObjectId(userId),
						_id: new Types.ObjectId(req.body.planId),
					},
				};

				Plan.aggregate([
					matchQuery,
				]).exec((err, plans: any[]) => {
					if (err) {
						// console.log(err);
						res.status(404).send({
							description: 'Error while creating trainer plan',
						});
						return;
					}

					if (!plans || plans.length === 0) {
						res.status(404).send({
							description: 'Requested plan not found',
						});
						return;
					}

					// Updating the plan, which now becomes compiled
					const plan: Plan = plans[0];
					plan.compiled = true;
					plan.schedules = newSchedules;

					Plan.findByIdAndUpdate(plan._id, plan, (updateErr, updatedPlan) => {
						if (updateErr) {
							// console.log(updateErr);
							res.status(404).send({
								description: 'Error while updating trainer plan',
							});
							return;
						}

						// We must send a notification to admins
						const notification = NotificationManager.sendPlanApprovalRequestNotification(plan._id);
						Notification.create(notification).then((savedNotification) => {
							if (savedNotification) {
								// console.log('Sent and saved notification (PlanApprovalRequest)');
								// console.log(savedNotification);
							}
						});

						res.status(200).json({
							description: 'Plan created succesfully and waiting to be approved',
						});
					});

				});

			}).catch((err: any) => {
				if (err) {
					// console.log(err);
					res.status(404).send({
						description: 'Error while creating trainer plan',
					});
					return;
				}
			});
		};
	}

}
