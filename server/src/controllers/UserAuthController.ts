import { Document, Schema, Model, model } from 'mongoose';
import bcrypt from 'bcryptjs';
import { Request, Response, NextFunction } from 'express';
import { User, BaseUser } from '@/models/User';
import AuthMiddleware from './AuthMiddleware';
import { Trainer } from '@/models/Trainer';
import UserController from './UserController';
import TrainersController from './TrainersController';
import { Admin } from '@/models/Admin';
import NotificationManager from './NotificationManager';
import { Notification } from '@/models/Notification';

export default abstract class UserAuthController extends AuthMiddleware {

	public static loginUser() {
		return (req: Request, res: Response, next: NextFunction) => {
			// console.log(req.body);

			if (!req.body) {
				res.status(500).json({
					description: 'Request body has not been set',
				});
				return;
			}

			if (!req.body.email || !req.body.password) {
				res.status(400).json({
					description: 'Email and password are missing',
				});
				return;
			}

			// Retrieve user if the user exist
			User.aggregate([
				{
					$match: {
						email: req.body.email,
					},
				},
			]).exec((err: any, users: User[]) => {
				if (users && users.length === 1) {
					this.checkUserPassword(users[0], req.body.password, res);
				} else {
					res.status(400).json({
						description: 'No user found with the provided credentials',
					});
					return;
				}
			});
		};
	}

	public static loginTrainer() {
		return (req: Request, res: Response, next: NextFunction) => {
			if (!req.body) {
				res.status(500).json({
					description: 'Request body has not been set',
				});
				return;
			}

			if (!req.body.email || !req.body.password) {
				res.status(400).json({
					description: 'Email and password are missing',
				});
				return;
			}

			// Retrieve trainer if the trainer exist
			Trainer.aggregate([
				{
					$match: {
						email: req.body.email,
					},
				},
			]).exec((err: any, trainers: Trainer[]) => {
				if (trainers && trainers.length === 1) {
					this.checkTrainerPassword(trainers[0], req.body.password, res);
				} else {
					res.status(400).json({
						description: 'No trainer found with the provided credentials',
					});
					return;
				}
			});
		};
	}

	public static loginAdmin() {
		return (req: Request, res: Response, next: NextFunction) => {
			if (!req.body) {
				res.status(500).json({
					description: 'Request body has not been set',
				});
				return;
			}

			if (!req.body.email || !req.body.password) {
				res.status(400).json({
					description: 'Email and password are missing',
				});
				return;
			}

			// Retrieve Admin if the trainer exist
			Admin.aggregate([
				{
					$match: {
						email: req.body.email,
					},
				},
			]).exec((err: any, admins: Admin[]) => {
				if (admins && admins.length === 1) {
					this.checkAdminPassword(admins[0], req.body.password, res);
				} else {
					res.status(400).json({
						description: 'No admin found with the provided credentials',
					});
					return;
				}
			});
		};
	}

	public static logoutUser() {
		return (req: Request, res: Response, next: NextFunction) => {
			// console.log(req.user);
			const user: BaseUser = req.user;
			const isAdmin = user.isAdmin;
			const isTrainer = user.isTrainer;

			const callback = (err: any, loggingOutUser: BaseUser) => {
				if (err) {
					// console.log(err);
				}
				if (loggingOutUser) {
					loggingOutUser.tokens = [];
					loggingOutUser.save();
				}
			};

			if (user) {
				if (isAdmin) {
					Admin.findById(user._id, callback);
				} else if (isTrainer) {
					Trainer.findById(user._id, callback);
				} else {
					User.findById(user._id, callback);
				}
			}
			// This invalidates our cookie if one exists
			req.logout();
			// Destroy session
			req.session = undefined;
			res.status(200)
				.clearCookie('virtualtraining.sid', { path: '/' })
				.json({ description: 'User logged out successfully' });
			// console.log('User logged out');
		};
	}

	public static createUser() {
		return this.createAnyUser(false, false);
	}

	public static createTrainer() {
		return this.createAnyUser(true, false);
	}

	public static createAdmin() {
		return this.createAnyUser(false, true);
	}

	public static createAnyUser(isTrainer: boolean, isAdmin: boolean) {
		return (req: Request, res: Response, next: NextFunction) => {
			// console.log(req.body);
			const userText = isAdmin ? 'Admin' : isTrainer ? 'Trainer' : 'User';

			if (!req.body) {
				res.status(500).json({
					description: 'Request body has not been set',
				});
				return;
			}

			if (isAdmin) {
				// Admin check
				if (!req.body.email
					|| !req.body.password
					|| !req.body.firstName
					|| !req.body.lastName) {
					res.status(400).json({
						description: 'Some required fields are missing',
					});
					return;
				}
			} else {
				// User and Trainer check
				if (!req.body.email
					|| !req.body.password
					|| !req.body.firstName
					|| !req.body.lastName
					|| req.body.sex == null || req.body.sex === undefined
					|| !req.body.birthdate
					|| !req.body.address) {
					res.status(400).json({
						description: 'Some required fields are missing',
					});
					return;
				}
			}

			if (req.body.password.length < 4) {
				res.status(400).json({
					description: 'Password must be at least 4',
				});
				return;
			}

			const checkQuery = {
				email: req.body.email,
			};

			// Main callback
			const callback = (users: any[]) => {
				// console.log(users);
				if (users != null && users.length > 0) {
					// The email already exists
					res.status(400).json({
						description: 'This email has already been used, please insert another one',
					});
					return;
				} else {
					// Create new user
					const now = new Date();
					let newUser: BaseUser;
					if (isAdmin) {
						newUser = new Admin({
							email: req.body.email,
							password: req.body.password,
							firstName: req.body.firstName,
							lastName: req.body.lastName,
							isAdmin: true,
							isTrainer: false,
							createdAt: now,
							updatedAt: now,
							tokens: [],
						});
					} else if (isTrainer) {
						// Finding muscles and trainer tags
						const musclesIds = req.body.muscles ? req.body.muscles : [];
						const trainerTagIds = req.body.trainerTags ? req.body.trainerTags : [];

						newUser = new Trainer({
							email: req.body.email,
							password: req.body.password,
							firstName: req.body.firstName,
							lastName: req.body.lastName,
							sex: req.body.sex,
							birthdate: req.body.birthdate,
							address: req.body.address,
							trainerTags: trainerTagIds,
							muscles: musclesIds,
							isAdmin: false,
							isTrainer: true,
							createdAt: now,
							updatedAt: now,
							tokens: [],
							accepted: false,
							pending: true,
						});
					} else {
						newUser = new User({
							email: req.body.email,
							password: req.body.password,
							firstName: req.body.firstName,
							sex: req.body.sex,
							lastName: req.body.lastName,
							birthdate: req.body.birthdate,
							address: req.body.address,
							isAdmin: false,
							isTrainer: false,
							createdAt: now,
							updatedAt: now,
							tokens: [],
						});
					}

					// console.log(newUser);

					// First param: salt size
					// Second param: callback called when the salt has been generated
					bcrypt.genSalt(10, (saltErr, salt) => {
						if (saltErr) {
							// console.log(saltErr);
							res.status(400).json({
								description: 'Error while generating salt',
							});
							return;
						}

						// Hash requires password, salt and the callback called when the has has been generated
						bcrypt.hash(newUser.password, salt, (hashError, hash) => {
							if (hashError) {
								// console.log(hashError);
								res.status(400).json({
									description: 'Error while generating hash',
								});
								return;
							}

							// Saving password and salt
							newUser.password = hash;
							newUser.salt = salt;
							// User has been created, we create a token to manage first authentication
							const generatedToken = this.generateJWTToken(newUser);
							newUser.tokens.push(generatedToken);

							// Creation callback
							const creationErrCallback = (saveErr: any) => {
								// console.log(saveErr);
								res.status(400).json({
									description: 'Error while saving the new ' + userText,
								});
								return;
							};

							if (isAdmin) {
								Admin.create(newUser).then((savedAdmin) => {
									// console.log('Saved admin ' + savedAdmin.email);
									res.status(201).json({
										_id: savedAdmin.id,
										email: savedAdmin.email,
										token: generatedToken,
									});
								}).catch(creationErrCallback);
							} else if (isTrainer) {
								Trainer.create(newUser).then((savedTrainer) => {
									// console.log('Saved trainer ' + savedTrainer.email);

									// A new Trainer has been created, we must send a notification to the admins
									const notification = NotificationManager.sendTrainerRegistrationNotification(savedTrainer._id);
									Notification.create(notification).then((savedNotification) => {
										if (savedNotification) {
											// console.log('Sent and saved notification (TrainerRegistration)');
											// console.log(savedNotification);
										}
									});


									res.status(201).json({
										_id: savedTrainer.id,
										email: savedTrainer.email,
										firstName: savedTrainer.firstName,
										lastName: savedTrainer.lastName,
										sex: savedTrainer.sex,
										birthdate: savedTrainer.birthdate,
										address: savedTrainer.address,
										trainerTags: savedTrainer.trainerTags,
										muscles: savedTrainer.muscles,
										rating: savedTrainer.rating,
										token: generatedToken,
									});
								}).catch(creationErrCallback);
							} else {
								User.create(newUser).then((savedUser) => {
									// console.log('Saved user ' + newUser.email);
									res.status(201).json({
										_id: savedUser.id,
										email: savedUser.email,
										firstName: savedUser.firstName,
										lastName: savedUser.lastName,
										sex: savedUser.sex,
										birthdate: savedUser.birthdate,
										address: savedUser.address,
										token: generatedToken,
									});
								}).catch(creationErrCallback);
							}
						});
					});
				}
			};

			// Failure callback
			const catchCallback = (findUserErr: any) => {
				if (findUserErr) {
					// console.log(findUserErr);
				}
				res.status(500).json({
					description: 'An error occured while trying to register the ' + userText,
				});
			};

			if (isAdmin) {
				// Admin creation
				Admin.find(checkQuery).then(callback).catch(catchCallback);
			} else if (isTrainer) {
				// Trainer creation
				Trainer.find(checkQuery).then(callback).catch(catchCallback);
			} else {
				// User creation
				User.find(checkQuery).then(callback).catch(catchCallback);
			}
		};
	}

	private static checkUserPassword(baseUser: BaseUser, passwordToBeTested: string, res: Response) {
		return this.checkPassword(baseUser, passwordToBeTested, false, false, res);
	}

	private static checkTrainerPassword(baseUser: BaseUser, passwordToBeTested: string, res: Response) {
		return this.checkPassword(baseUser, passwordToBeTested, true, false, res);
	}

	private static checkAdminPassword(baseUser: BaseUser, passwordToBeTested: string, res: Response) {
		return this.checkPassword(baseUser, passwordToBeTested, false, true, res);
	}

	// tslint:disable-next-line: max-line-length
	private static checkPassword(baseUser: BaseUser, passwordToBeTested: string, isTrainer: boolean, isAdmin: boolean, res: Response) {
		// Found the user, we must check the password
		// console.log(baseUser);

		const userSalt = baseUser.salt;
		const userPassword = baseUser.password;

		// Hash requires password, salt and the callback called when the has has been generated
		bcrypt.hash(passwordToBeTested, userSalt, (hashError, hash) => {
			if (hashError) {
				// console.log(hashError);
				res.status(400).json({
					description: 'Error while generating hash',
				});
				return;
			}

			// console.log(hash);
			// console.log(userPassword);

			// Check if the password are the same
			if (userPassword !== hash) {
				res.status(400).json({
					description: 'No ' + (isTrainer ? 'trainer' : 'user') + ' found with the provided credentials',
				});
				return;
			}

			// User has been authenticated, we create a token
			const generatedToken = this.generateJWTToken(baseUser);
			baseUser.tokens = [];
			baseUser.tokens.push(generatedToken);

			// Creating callback
			const callback = (saveErr: any, savedUser: any) => {
				if (saveErr) {
					// console.log(saveErr);
					res.status(400).json({
						description: 'Error while saving generated credentials',
					});
					return;
				}
				return res.status(200).json({
					description: 'Login successfully',
					_id: baseUser._id,
					token: generatedToken,
				});
			};

			// Save the user with the new token
			if (isAdmin) {
				Admin.findByIdAndUpdate(baseUser._id, baseUser, callback);
			} else {
				if (isTrainer) {
					Trainer.findByIdAndUpdate(baseUser._id, baseUser, callback);
				} else {
					User.findByIdAndUpdate(baseUser._id, baseUser, callback);
				}
			}
		});
	}
}
