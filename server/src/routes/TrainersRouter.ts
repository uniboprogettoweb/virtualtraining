import { Application } from 'express';
import TrainersController from '@/controllers/TrainersController';
import UserAuthController from '@/controllers/UserAuthController';
import UserController from '@/controllers/UserController';

export default abstract class TrainersRouter {
	public static setupRoutes(app: Application) {
		app.route('/api/trainers')
			.get(TrainersController.getTrainers());

		app.route('/api/trainers/info')
			.get(UserAuthController.validateUser(), TrainersController.getTrainerInfo());

		app.route('/api/trainers/info')
			.post(UserAuthController.validateUser(), UserController.postUpdateTrainerInfo());

		app.route('/api/trainers/plans')
			.get(UserAuthController.validateUser(), TrainersController.getTrainerPlans());

		app.route('/api/trainers/create-plan')
			.post(UserAuthController.validateUser(), TrainersController.postCreatePlan());

		app.route('/api/trainers/:id')
			.get(TrainersController.getTrainer());

	}
}
