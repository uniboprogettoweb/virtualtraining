import { Application } from 'express';
import CoachInfoController from '@/controllers/CoachInfoController';


export default abstract class CoachInfoRouter {
	public static setupRoutes(app: Application) {

    app.route('/api/coachInfo')
    .get(CoachInfoController.coachInfo());
	}
}
