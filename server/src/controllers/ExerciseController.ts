import { Document, Schema, Model, model, Types } from 'mongoose';
import { Request, Response } from 'express';
import { Exercise } from '@/models/Exercise';

export default abstract class ExerciseController {

    public static EXERCISE_MUSCLES_LOOKUP = {
		$lookup: {
			from: 'muscles',
			localField: 'muscles',
			foreignField: 'muscleId',
			as: 'muscles',
		},
    };

    public static getExercises() {
		return (req: Request, res: Response) => {
            Exercise.aggregate([
                this.EXERCISE_MUSCLES_LOOKUP,
            ]).exec((err: any, exercises: Exercise[]) => {
                if (err) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Exercises not found',
                    });
                    return;
                }

                res.status(200).send(exercises);
            });
		};
    }

    public static createExercise() {
		return (req: Request, res: Response) => {
            // console.log(req.body);
            if (!req.body || !req.body.name || !req.body.description || !req.body.muscles) {
                res.status(404).send({
                    description: 'Must specify name, description, muscles and isTimed(optional)',
                });
                return;
            }

            Exercise.find().sort({exerciseId: -1}).limit(1).exec((err: any, exercises: Exercise[]) => {
                if (err) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Error while creating a new exercise',
                    });
                    return;
                }

                let newExerciseId = 1;
                if (exercises.length !== 0) {
                    newExerciseId = exercises[0].exerciseId + 1;
                }

                const newExercise = new Exercise({
                    exerciseId: newExerciseId,
                    name: req.body.name,
                    description: req.body.description,
                    isTimed: req.body.isTimed,
                    muscles: req.body.muscles,
                    isMapNeeded: req.body.isMapNeeded,
                    isWeightNeeded: req.body.isWeightNeeded,
                    exerciseImage: req.body.exerciseImage,
                });
                newExercise.save((createErr: any, createdExercise: Exercise) => {
                    if (createErr) {
                        // console.log(createErr);
                        res.status(404).send({
                            description: 'Error while creating a new exercise',
                        });
                        return;
                    }

                    res.status(201).send(createdExercise);
                });
            });
        };
    }

    public static updateExercise() {
		return (req: Request, res: Response) => {
            // console.log(req.body);
            if (!req.body
                || !req.body.name
                || !req.body.description
                || !req.body.muscles
                || req.body.isTimed  == null || req.body.isTimed === undefined
                || req.body.isMapNeeded  == null || req.body.isMapNeeded === undefined
                || req.body.isWeightNeeded  == null || req.body.isWeightNeeded === undefined
                || !req.body.exerciseImage
                || !req.params.exerciseId) {
                res.status(404).send({
                    // tslint:disable-next-line: max-line-length
                    description: 'Must specify an exerciseId in path params and name, description, muscles, isTimed, isMapNeede, isWeightNeeded and exerciseImage in body',
                });
                return;
            }

            const exerciseId = req.params.exerciseId;
            const name = req.body.name;
            const description = req.body.description;
            const isTimed = req.body.isTimed;
            const muscles = req.body.muscles;
            const isMapNeeded = req.body.isMapNeeded;
            const isWeightNeeded = req.body.isWeightNeeded;
            const exerciseImage = req.body.exerciseImage;

            Exercise.findOneAndUpdate({
                exerciseId,
            }, {
                name,
                description,
                isTimed,
                muscles,
                isMapNeeded,
                isWeightNeeded,
                exerciseImage,
            }, {
                upsert: false,
            }, (err, oldExercise) => {
                if (err || !oldExercise) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Error while updating the exercise',
                    });
                    return;
                }

                res.status(200).send({
                    _id: oldExercise._id,
                    exerciseId: oldExercise.exerciseId,
                    name,
                    description,
                    isTimed,
                    muscles,
                    isMapNeeded,
                    isWeightNeeded,
                    exerciseImage,
                });
            });
		};
    }

    public static deleteExercise() {
		return (req: Request, res: Response) => {
            if (!req.params.exerciseId) {
                res.status(404).send({
                    description: 'Must specify an exerciseId in path params',
                });
                return;
            }

            const exerciseId = req.params.exerciseId;

            Exercise.findOneAndDelete({
                exerciseId,
            }, (err, oldExercise) => {
                if (err || !oldExercise) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Error while deleting the exercise',
                    });
                    return;
                }

                res.status(200).send({description: 'Exercise deleted successfully'});
            });
		};
    }
}
