import { Document, Schema, Model, model } from 'mongoose';
import { User, BaseUser } from './User';
import { Trainer } from './Trainer';
import { Admin } from './Admin';
import { Plan } from './Plan';

export interface Notification extends Document {
    notificationType: NotificationType;
    userType: UserType;
    receiverId: BaseUser;
    title: string;
    description: string;
    date: Date;
    userId: BaseUser;
    planId: Plan;
    readBy: BaseUser[];
    deletedBy: BaseUser[];
}

export enum UserType {
    ALL = 'all',
    ADMIN = 'admin',
    TRAINER = 'trainer',
    USER = 'user',
    NONE = 'none',
}

export enum NotificationType {
    CUSTOM = 1,
    NEW_TRAINER_REGISTRATION = 2,
    PLAN_APPROVAL_REQUEST = 3,
    NEW_PLAN_REQUEST = 4,
    TRAINER_ACCEPTED = 5,
    TRAINER_REFUSED = 6,
    TRAINER_PLAN_ACCEPTED = 7,
    TRAINER_PLAN_REFUSED = 8,
    PLAN_CREATED = 9,
}

const NotificationSchema: Schema = new Schema({
    notificationType: { type: Number, default: NotificationType.CUSTOM},
    userType: { type: String, default: UserType.NONE },
    receiverId: { type: Schema.Types.ObjectId, ref: User || Trainer || Admin },
    title: { type: String },
    description: { type: String },
    date: { type: Date, default: new Date()},
    userId: { type: Schema.Types.ObjectId, ref: User || Trainer || Admin },
    planId: { type: Schema.Types.ObjectId, ref: Plan },
    readBy: [{ type: Schema.Types.ObjectId, ref: User || Trainer || Admin }],
    deletedBy: [{ type: Schema.Types.ObjectId, ref: User || Trainer || Admin }],
});

// Export the Notification model and return the Notification interface
export const Notification: Model<Notification> = model<Notification>('Notification', NotificationSchema);
