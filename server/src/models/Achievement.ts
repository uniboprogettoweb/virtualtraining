import { Document, Schema, Model, model } from 'mongoose';

export interface Achievement extends Document {
    achievementId: number;
    name: string;
    description: string;
    icon: string;
    card: object;
}

const AchievementSchema: Schema = new Schema({
    achievementId: { type: Number, required: true, unique: true },
    name: { type: String, required: true },
    description: { type: String, required: true },
    icon: { type: String },
    card: { type: Schema.Types.Mixed, required: true },
});

// Export the Achievement model and return the Achievement interface
// tslint:disable-next-line: max-line-length
export const Achievement: Model<Achievement> = model<Achievement>('Achievement', AchievementSchema);
