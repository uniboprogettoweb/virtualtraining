import AuthMiddleware from './AuthMiddleware';
import { Request, Response, NextFunction } from 'express';
import { Plan } from '@/models/Plan';
import TrainersController from './TrainersController';
import { Trainer } from '@/models/Trainer';
import { Types } from 'mongoose';
import NotificationManager from './NotificationManager';
import { Notification } from '@/models/Notification';

export default abstract class AdminController extends AuthMiddleware {

    public static getPendingPlans() {
        return (req: Request, res: Response, next: NextFunction) => {
            if (!req.user) {
                // console.log('Admin info not passed to getPendingPlans()');
                res.status(404).send({
                    description: 'Admin not found, the requested _id seems incorrect',
                });
                return;
            }

            // Retrieving user id
            const userId = req.user._id;

            Plan.aggregate([
                {
                    $match: {
                        pending: true,
                        compiled: true,
                        approved: false,
                    },
                },
				TrainersController.TRAINER_TRAINERTAGS_LOOKUP,
				TrainersController.TRAINER_MUSCLES_LOOKUP,
            ]).exec((err, plans: any[]) => {
                if (err) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Error while retrieving pending plans',
                    });
                    return;
                }

                res.status(200).send(plans);
            });
        };
    }

    public static getPendingTrainers() {
        return (req: Request, res: Response, next: NextFunction) => {
            if (!req.user) {
                // console.log('Admin info not passed to getPendingTrainers()');
                res.status(404).send({
                    description: 'Admin not found, the requested _id seems incorrect',
                });
                return;
            }

            // Retrieving user id
            const userId = req.user._id;
            Trainer.aggregate([
                {
                    $match: {
                        pending: true,
                        accepted: false,
                    },
                },
                TrainersController.TRAINER_TRAINERTAGS_LOOKUP,
                TrainersController.TRAINER_MUSCLES_LOOKUP,
            ]).exec((err, trainers: any[]) => {
                if (err) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Error while retrieving pending trainers',
                    });
                    return;
                }
                const parsedTrainers = trainers.map((t) => TrainersController.parseTrainer(t));
                res.status(200).send(parsedTrainers);
            });
        };
    }

    // tslint:disable: align
    // tslint:disable: max-line-length
    public static postValidateTrainer() {
        return (req: Request, res: Response, next: NextFunction) => {
            if (!req.user) {
                // console.log('Admin info not passed to postValidateTrainer()');
                res.status(404).send({
                    description: 'Admin not found, the requested _id seems incorrect',
                });
                return;
            }

            if (!req.body) {
			    res.status(500).json({
					description: 'Request body has not been set',
                });
                return;
			}

            if (!req.body.trainerId || req.body.status == null || req.body.status === undefined) {
				res.status(400).json({
					description: 'trainedId or status are missing',
				});
				return;
			}

            // Retrieving user id
            const userId = req.user._id;
            Trainer.findById(req.body.trainerId, (err, trainer: Trainer) => {
                if (err) {
                    res.status(404).send({
                        description: 'Error while updating trainer status',
                    });
                    return;
                }

                // Setting the trainer status
                if (trainer) {
                    trainer.pending = false;
                    trainer.accepted = req.body.status;
                    trainer.save((saveErr, savedTrainer) => {
                        if (saveErr) {
                            res.status(404).send({
                                description: 'Error while saving trainer status',
                            });
                            return;
                        }

                          // We must send a notification to the trainer
                        // If the plan has been approved, we must send a notification to the user
                        const trainerStatusNotification = trainer.accepted ? NotificationManager.sendTrainerAcceptedNotification(trainer._id) : NotificationManager.sendTrainerRefusedNotification(trainer._id);
                        Notification.create(trainerStatusNotification).then((savedNotification) => {
                            if (savedNotification) {
                                const logString = trainer.accepted ? 'TrainerAccepted' : 'TrainerRefused';
                                // console.log('Sent and saved notification (' + logString + ')');
                                // console.log(savedNotification);
                            }
                        });

                        res.status(200).json({
                            description: 'Trainer accepted status set to ' + req.body.status,
                        });
                    });
                }
            });
        };
    }

    // tslint:disable: max-line-length
    public static postValidatePlan() {
        return (req: Request, res: Response, next: NextFunction) => {
            if (!req.user) {
                // console.log('Admin info not passed to postValidatePlan()');
                res.status(404).send({
                    description: 'Admin not found, the requested _id seems incorrect',
                });
                return;
            }

            if (!req.body) {
			    res.status(500).json({
					description: 'Request body has not been set',
                });
       return;
			}

            if (!req.body.planId || req.body.status == null || req.body.status === undefined) {
				res.status(400).json({
					description: 'planId or status are missing',
				});
				return;
			}

            // Retrieving user id
            const userId = req.user._id;
            Plan.findById(req.body.planId, (err, plan: Plan) => {
                if (err) {
                    res.status(404).send({
                        description: 'Error while updating trainer status',
                    });
                    return;
                }

                // Setting the trainer status
                if (plan) {
                    plan.pending = false;
                    plan.approved = req.body.status;
                    plan.save((saveErr, savedPlan) => {
                        if (saveErr) {
                            res.status(404).send({
                                description: 'Error while saving plan status',
                            });
                            return;
                        }

                        // We must send a notification to the trainer
                        // If the plan has been approved, we must send a notification to the user
                        if (plan.approved) {
                            const trainerPlanApprovedNotification = NotificationManager.sendTrainerPlanAcceptedNotification(plan.compiledBy._id, plan._id);
                            Notification.create(trainerPlanApprovedNotification).then((savedNotification) => {
                                if (savedNotification) {
                                    // console.log('Sent and saved notification (TrainerPlanAccepted)');
                                    // console.log(savedNotification);
                                }
                            });

                            const planCreatedNotification = NotificationManager.sendPlanCreatedNotification(plan.associatedTo._id, plan._id);
                            Notification.create(planCreatedNotification).then((savedNotification) => {
                                if (savedNotification) {
                                    // console.log('Sent and saved notification (PlanCreated)');
                                    // console.log(savedNotification);
                                }
                            });

                        } else {
                            const trainerPlanRefusedNotification = NotificationManager.sendTrainerPlanRefusedNotification(plan.compiledBy._id, plan._id);
                            Notification.create(trainerPlanRefusedNotification).then((savedNotification) => {
                                if (savedNotification) {
                                    // console.log('Sent and saved notification (TrainerPlanRefused)');
                                    // console.log(savedNotification);
                                }
                            });
                        }


                        res.status(200).json({
                            description: 'Plan accepted status set to ' + req.body.status,
                        });
                    });
                }
            });
        };
    }
}
