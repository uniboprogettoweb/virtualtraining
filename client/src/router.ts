import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Shop from './views/Shop/Shop.vue';
import ShopCart from './views/Cart/ShopCart.vue';
import CompletePlan from './views/CompletePlan.vue';
import UserRequest from './views/UserRequest.vue';
import UserPlan from './views/UserPlan.vue';
import UserPlanExecution from './views/UserPlanExecution.vue';
import Leaderboard from './views/Leaderboard.vue';
import User from './views/User.vue';
import SignUnSignIn from './views/SignUpSignIn.vue';
import CoachProfile from './views/CoachProfile.vue';
import CoachPlan from './views/CoachPlan.vue';
import Admin from './views/Admin.vue';
import SelectRequest from './views/SelectRequest.vue';

import EditAddProduct from './components/CRUD/Product/EditProductsCollection/EditAddProducts.vue';
import EditUser from './components/CRUD/User/EditUserCollection/EditUser.vue';
import CRUD from './views/CRUD.vue';

Vue.use(Router);

// Pages (name) that require user permissions
export const permissionLevelUser: string[] = ['cart', 'userRequest', 'userPlan', 'userPlanExecution', 'user'];

// Pages (name) that require coach permissions
export const permissionLevelCoach: string[] = ['completePlan', 'coachProfile', 'coachPlans', 'selectRequest'];

// Pages (name) that require user permissions
export const permissionLevelAdmin: string[] = ['trainerApprovation', 'planApprovation', 'addProduct', 'editUser', 'crud', 'admin'];

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About/About.vue'),
    },

    {
      path: '/admin',
      name: 'admin',
      component: Admin,
    },
    {
      path: '/completePlan',
      name: 'completePlan',
      component: CompletePlan,
    },

	 {
      path: '/userRequest',
      name: 'userRequest',
      component: UserRequest,
    },
	{
      path: '/userPlan',
      name: 'userPlan',
      component: UserPlan,
    },
	{
      path: '/userPlanExecution',
      name: 'userPlanExecution',
      props: true, // Enable passing prop throught router-link
      component: UserPlanExecution,
    },

	{
      path: '/leaderboard',
      name: 'leaderboard',
      component: Leaderboard,
    },
	{
      path: '/user',
      name: 'user',
      component: User,
    },
    {
      path: '/shop',
      name: 'shop',
      component: Shop,
    },
    {
      path: '/cart',
      name: 'cart',
      component: ShopCart,
    },
    {
      path: '/user-access',
      name: 'user-access',
      component: SignUnSignIn,
    },
    {
      path: '/coachProfile',
      name: 'coachProfile',
      component: CoachProfile,
    },
    {
      path: '/selectRequest',
      name: 'selectRequest',
      component: SelectRequest,
    },

    {
      path: '/coachPlans',
      name: 'coachPlans',
      component: CoachPlan,
    },
    {
      path: '/add-product/:productToEdit',
      name: 'addProduct',
      props: true, // Enable passing prop throught router-link
      component: EditAddProduct,
    },
    {
      path: '/edit-user/:userToEdit',
      name: 'editUser',
      props: true, // Enable passing prop throught router-link
      component: EditUser,
    },
    {
      path: '/crud',
      name: 'crud',
      component: CRUD,
    },
  ],
});
