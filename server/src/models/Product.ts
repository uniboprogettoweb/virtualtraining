import { Document, Schema, Model, model } from 'mongoose';
import { Exercise } from './Exercise';

export interface Product extends Document {
    productId: number;
    categoryId: number;
    name: string;
    description: string;
    imageOne: string;
    imageTwo: string;
    price: number;
    discountedPrice: number;
    onSale: boolean;
    exercises: [Exercise];
}

const ProductSchema: Schema = new Schema({
    productId: { type: Number, required: true},
    categoryId: { type: Number, required: true},
    name: { type: String, required: true },
    description: { type: String },
    imageOne: { type: String },
    imageTwo: { type: String },
    price: { type: Number, required: true },
    discountedPrice: { type: Number, default: 0.0 },
    onSale: { type: Boolean },
    exercises: { type: Array, required: true, default: []},
});

// Export the Product model and return the Product interface
export const Product: Model<Product> = model<Product>('Product', ProductSchema);
