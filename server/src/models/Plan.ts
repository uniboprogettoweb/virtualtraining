import { Document, Schema, Model, model } from 'mongoose';
import { Muscle } from './Muscle';
import { User } from './User';
import { Trainer } from './Trainer';
import { Schedule } from './Schedule';

export interface Plan extends Document {
    associatedTo: User;
    compiledBy: Trainer;
    approved: boolean;
    pending: boolean;
    compiled: boolean;
    schedules: Schedule[];
    rating: number;
    isRated: boolean;
    timetable: string;
    description: string;
    muscles: [Muscle];
    trainerTags: [Muscle];
}

const PlanSchema: Schema = new Schema({
    associatedTo: { type: Schema.Types.ObjectId, ref: User, required: true},
    compiledBy: { type: Schema.Types.ObjectId, ref: Trainer, required: true},
    approved: { type: Boolean , required: true, default: false },
    pending: { type: Boolean , required: true, default: true },
    compiled: { type: Boolean , required: true, default: false },
    schedules: { type: Array, required: true, default: []},
    rating: { type: Number, default: 0 },
    isRated: { type: Boolean, default: false },
    timetable: { type: String },
    description: { type: String },
    muscles: { type: Array, required: true, default: []},
    trainerTags: { type: Array, required: true, default: []},
});

// Export the Plan model and return the Plan interface
export const Plan: Model<Plan> = model<Plan>('Plan', PlanSchema);
