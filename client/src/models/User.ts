import Achievement from './Achievement';

export default interface User {
    _id: string;
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    birthdate: Date;
    address: string;
    profileImage: string;
    level: number;
    experience: number;
    achievements: Achievement[];
}

/**
 * Interface for create new user
 */
export interface NewUser {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  birthdate: Date;
  address: string;
  profileImage: string;
  level: number;
  experience: number;
  achievements: Achievement[];
  sex: number;
  isACoach: boolean;
  trainerTags: number[];
  muscles: number[];
}

export interface UserWithId extends NewUser {
  _id: string;
}


export interface UpdatedUser {
  _id: string;
  // oldPassword: string;
  // password: string;
  firstName: string;
  lastName: string;
  birthdate: Date;
  address: string;
}
