import { Document, Schema, Model, model } from 'mongoose';
import { Request, Response } from 'express';
import { Category } from '@/models/Category';
import { Product } from '@/models/Product';

export default abstract class ProductController {

    public static PRODUCTS_CATEGORY_LOOKUP = {
        $lookup: {
            from: 'categories',
            localField: 'categoryId',
            foreignField: 'categoryId',
            as: 'category',
        },
    };

    public static PRODUCTS_EXERCISES_LOOKUP = {
        $lookup: {
            from: 'exercises',
            localField: 'exercises',
            foreignField: 'exerciseId',
            as: 'exercises',
        },
    };

    public static getProducts() {
        return (req: Request, res: Response) => {
            const categoriesIds: any = req.query.categories ? JSON.parse(req.query.categories) : null;
            // console.log(categoriesIds);

            const callback = (err: any, products: Product[]) => {
                if (err) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Products not found',
                    });
                    return;
                }

                res.status(200).send(products);
            };

            const aggregateQuery = [];
            if (categoriesIds) {
                const matchQuery: any = {};
                matchQuery.$match = {
                    categoryId: { $in: categoriesIds },
                };
                // console.log(matchQuery);
                aggregateQuery.push(matchQuery);
            }

            aggregateQuery.push(this.PRODUCTS_EXERCISES_LOOKUP);
            aggregateQuery.push(this.PRODUCTS_CATEGORY_LOOKUP);

            Product.aggregate(aggregateQuery).exec(callback);
        };
    }

    public static getRecommendedProducts() {
        return (req: Request, res: Response) => {
            const exercisesIds = req.query.exercises ? JSON.parse(req.query.exercises) : null;

            if (exercisesIds == null || exercisesIds === undefined || exercisesIds.length === 0) {
                res.status(404).send({
                    description: 'The exercises were not provided',
                });
                return;
            }
            // console.log(exercisesIds);

            Product.aggregate([
                {
                    $match: {
                        exercises: { $in: exercisesIds },
                    },
                },
                this.PRODUCTS_EXERCISES_LOOKUP,
                this.PRODUCTS_CATEGORY_LOOKUP,
            ]).exec((err: any, products: Product[]) => {
                // console.log(products);
                if (err) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Products not found',
                    });
                } else {
                    if (products == null) {
                        res.status(404).send({
                            description: 'Products not found',
                        });
                    } else {
                        res.status(200).json(products);
                    }
                }
            });
        };
    }

    public static createProduct() {
        return (req: Request, res: Response) => {
            // console.log(req.body);
            if (!req.body || !req.body.name || !req.body.categoryId || !req.body.price) {
                res.status(404).send({
                    description: 'Must specify name, categoryId, price',
                });
                return;
            }

            Product.find().sort({ productId: -1 }).limit(1).exec((err: any, products: Product[]) => {
                if (err) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Error while creating a new product',
                    });
                    return;
                }

                let newProductId = 1;
                if (products.length !== 0) {
                    newProductId = products[0].productId + 1;
                }

                const newProduct = new Product({
                    productId: newProductId,
                    name: req.body.name,
                    categoryId: req.body.categoryId,
                    description: req.body.description,
                    imageOne: req.body.imageOne,
                    imageTwo: req.body.imageTwo,
                    price: req.body.price,
                    discountedPrice: req.body.discountedPrice ? req.body.discountedPrice : req.body.price,
                    onSale: req.body.onSale,
                    exercises: req.body.exercises,
                });

                newProduct.save((saveErr, savedProduct: Product) => {
                    if (saveErr) {
                        // console.log(saveErr);
                        res.status(404).send({
                            description: 'Error while creating a new product',
                        });
                        return;
                    }
                    res.status(201).send(savedProduct);
                });

            });
        };
    }

    // tslint:disable: max-line-length
    public static updateProduct() {
        return (req: Request, res: Response) => {
            // console.log(req.body);
            if (!req.body
                || !req.body.name
                || !req.body.categoryId
                || !req.body.price
                || !req.params.productId) {
                res.status(404).send({
                    description: 'Must specify a productId in path params and atleast name, categoryId and price in body',
                });
                return;
            }

            const productId = req.params.productId;
            const name = req.body.name;
            const categoryId = req.body.categoryId;
            const description = req.body.description;
            const imageOne = req.body.imageOne;
            const imageTwo = req.body.imageTwo;
            const price = req.body.price;
            const discountedPrice = req.body.discountedPrice ? req.body.discountedPrice : req.body.price;
            const onSale = req.body.onSale;
            const exercises = req.body.exercises;

            Product.findOneAndUpdate({
                productId,
            }, {
                name,
                categoryId,
                description,
                imageOne,
                imageTwo,
                price,
                discountedPrice,
                onSale,
                exercises,
            }, {
                upsert: false,
            }, (err, oldProduct) => {
                if (err || !oldProduct) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Error while updating the product',
                    });
                    return;
                }

                res.status(200).send({
                    _id: oldProduct._id,
                    productId: oldProduct.productId,
                    name,
                    categoryId,
                    description,
                    imageOne,
                    imageTwo,
                    price,
                    discountedPrice,
                    onSale,
                    exercises,
                });
            });

        };
    }

    public static deleteProduct() {
		return (req: Request, res: Response) => {
            // console.log(req.body);
            if (!req.params.productId) {
                res.status(404).send({
                    description: 'Must specify an productId in path params',
                });
                return;
            }

            const productId = req.params.productId;

            Product.findOneAndDelete({
                productId,
            }, (err, oldProduct) => {
                if (err || !oldProduct) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Error while deleting the product',
                    });
                    return;
                }

                res.status(200).send({description: 'Product deleted successfully'});
            });
		};
    }
}
