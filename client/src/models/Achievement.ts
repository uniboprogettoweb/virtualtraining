import Exercise from './Exercise';

export default interface Achievement {
	achievementId: number;
    name: string;
	description: string;
	icon: string;
	card: Card;
}

export interface Card {
	amount: number;
	weight: number;
  exercise: Exercise;
}
