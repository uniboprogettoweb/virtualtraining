import { Application } from 'express';
import ShopController from '@/controllers/ShopController';
import AuthMiddleware from '@/controllers/AuthMiddleware';

export default abstract class ShopRouter {
	public static setupRoutes(app: Application) {
		app.route('/api/shop/user-cart')
			.get(AuthMiddleware.validateUser(), ShopController.getUserCart());

		app.route('/api/shop/update-cart-product')
			.post(AuthMiddleware.validateUser(), ShopController.postUpdateCartProduct());

		app.route('/api/shop/delete-user-cart')
			.delete(AuthMiddleware.validateUser(), ShopController.deleteUserCart());

	}
}
