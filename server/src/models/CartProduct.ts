import { Document, Schema, Model, model } from 'mongoose';
import { Card } from './Card';
import { Product } from './Product';

export interface CartProduct extends Document {
    productId: Product;
    quantity: number;
}

const CartProductSchema: Schema = new Schema({
    product: { type: Schema.Types.ObjectId, ref: Product, required: true},
    quantity: { type: Number, required: true },
});

// Export the CartProduct model and return the CartProduct interface
// tslint:disable-next-line: max-line-length
export const CartProduct: Model<CartProduct> = model<CartProduct>('CartProduct', CartProductSchema);
