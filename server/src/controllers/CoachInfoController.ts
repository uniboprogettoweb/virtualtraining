import { Document, Schema, Model, model } from 'mongoose';
import { Request, Response, NextFunction } from 'express';


import data_coach from '@/mock/coachInfo.json';


export default abstract class CoachInfoController {


  public static coachInfo() {
		return (req: Request, res: Response, next: NextFunction) => {
			res.json(data_coach);
		};
	}
}
