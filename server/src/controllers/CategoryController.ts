import { Document, Schema, Model, model } from 'mongoose';
import { Request, Response } from 'express';
import { Category } from '@/models/Category';

export default abstract class CategoryController {

    public static getCategories() {
		return (req: Request, res: Response) => {
			Category.find((err: any, categories: Category[]) => {
                if (err) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Categories not found',
                    });
                    return;
                }

                res.status(200).send(categories);
            });
		};
    }

    public static createCategory() {
		return (req: Request, res: Response) => {
            // console.log(req.body);
            if (!req.body || !req.body.name) {
                res.status(404).send({
                    description: 'Must specify a name',
                });
                return;
            }

            Category.find().sort({categoryId: -1}).limit(1).exec((err: any, categories: Category[]) => {
                if (err) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Error while creating a new category',
                    });
                    return;
                }

                let newCategoryId = 1;
                if (categories.length !== 0) {
                    newCategoryId = categories[0].categoryId + 1;
                }

                const newCategory = new Category({
                    categoryId: newCategoryId,
                    name: req.body.name,
                });
                newCategory.save((createErr: any, createdCategory: Category) => {
                    if (createErr) {
                        // console.log(createErr);
                        res.status(404).send({
                            description: 'Error while creating a new category',
                        });
                        return;
                    }

                    res.status(201).send(createdCategory);
                });
            });
		};
    }

}
