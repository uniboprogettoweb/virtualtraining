import Exercise from './Exercise';
export default interface Schedule {
  cards: Card[];
}

export interface Card {
	amount: number;
	weight: number;
  exercise: Exercise;
}
