import { Document, Schema, Model, model } from 'mongoose';

export interface Sensor extends Document {
    name: string;
    type: string;
    value: number;
    date: Date;
}

const SensorSchema: Schema = new Schema({
    name: { type: String },
    type: { type: String },
    value: { type: Number },
    date: { type: Date },
});

// Export the Sensor model and return the Sensor interface
export const Sensor: Model<Sensor> = model<Sensor>('Sensor', SensorSchema);
