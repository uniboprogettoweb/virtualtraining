import { Application } from 'express';
import ProductController from '@/controllers/ProductController';
import AuthMiddleware from '@/controllers/AuthMiddleware';

export default abstract class ProductRouter {
	public static setupRoutes(app: Application) {
        app.route('/api/shop/products')
            .get(ProductController.getProducts())
            .post(ProductController.createProduct());

        app.route('/api/shop/products/:productId')
        .put(ProductController.updateProduct())
        .delete(ProductController.deleteProduct());

        app.route('/api/shop/products/recommended')
            .get(AuthMiddleware.validateUser(), ProductController.getRecommendedProducts());
    }
}
