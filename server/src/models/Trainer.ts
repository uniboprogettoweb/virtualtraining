import { Document, Schema, Model, model } from 'mongoose';
import { BaseUser } from '@/models/User';

export interface Trainer extends BaseUser {
	firstName: string;
	lastName: string;
	sex: number;
	birthdate: Date;
	address: string;
	profileImage: string;
	description: string;
	accepted: boolean;
	pending: boolean;
	trainerTags: [number];
	muscles: [number];
	rating: number;
}

const TrainerSchema: Schema = new Schema({
	email: { type: String, required: true, unique: true },
	password: { type: String, required: true },
	firstName: { type: String, required: true },
	lastName: { type: String, required: true },
	sex: { type: Number, required: true, default: 0 },
	birthdate: { type: Date, required: true},
	address: { type: String, required: true},
	profileImage: { type: String, default: '' },
	description: { type: String, default: '' },
	accepted: { type: Boolean, default: false },
	pending: { type: Boolean, default: true },
	trainerTags: { type: Array, default: [] },
	muscles: { type: Array, default: [] },
	rating: { type: Number, min: 0, max: 5, default: 0},
	isTrainer: { type: Boolean, required: true, default: true },
	isAdmin: { type: Boolean, required: true, default: false },
	salt: { type: String, required: true},
	createdAt: Date,
	updatedAt: Date,
	tokens: { type: Array , required: true, default: [] },
});

// Update time related fields
TrainerSchema.pre<Trainer>('save', function(next) {
	if (this) {
		const now = new Date();
		if (!this.createdAt) {
			this.createdAt = now;
		}
		this.updatedAt = now;
	}
	next();
});

// Export the Trainer model and return the Trainer interface
export const Trainer: Model<Trainer> = model<Trainer>('Trainer', TrainerSchema);
