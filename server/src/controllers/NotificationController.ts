import { Document, Schema, Model, model, Types } from 'mongoose';
import { Request, Response, NextFunction } from 'express';
import UserController from './UserController';
import { User } from '@/models/User';
import { Notification, UserType } from '@/models/Notification';
import socketIO from 'socket.io';
import { ServerHttp2Session } from 'http2';
import NotificationFactory from './NotificationFactory';
import NotificationManager from './NotificationManager';

export default abstract class NotificationController {

    public static TEST_ID = '5da1dd2805894c43acb9c479';

    public static initWebsocketIO(server: any) {
        this.serverIO = socketIO(server);
        this.serverIO.on('connection', (socket) => {
            // console.log('A user connected: ' + socket.client.id);
        });
    }

    public static sendTestNotification() {
        return (req: Request, res: Response, next: NextFunction) => {

            const objectId = new Types.ObjectId(this.TEST_ID);

            NotificationManager.sendTrainerRegistrationNotification(objectId);
            NotificationManager.sendPlanApprovalRequestNotification(objectId);
            NotificationManager.sendNewPlanRequestNotification(objectId, objectId, objectId);
            NotificationManager.sendTrainerAcceptedNotification(objectId);
            NotificationManager.sendTrainerRefusedNotification(objectId);
            NotificationManager.sendTrainerPlanAcceptedNotification(objectId, objectId);
            NotificationManager.sendTrainerPlanRefusedNotification(objectId, objectId);
            NotificationManager.sendPlanCreatedNotification(objectId, objectId);

            res.status(200).send({ message: 'message' });
        };
    }

    public static sendNotification(event: string, notification: Notification | any) {
        // console.log('Sending notification at event: ' + event);
        // console.log('With content: ' + notification);
        this.serverIO.emit(event, notification);
    }

    public static getUserNotifications() {
        return (req: Request, res: Response, next: NextFunction) => {
            if (!req.user) {
                // console.log('User info not passed to getUserNotifications()');
                res.status(404).send({
                    description: 'User not found, the requested _id seems incorrect',
                });
                return;
            }
            // console.log(req.user);
            // Retrieving user id
            const userId = req.user._id;
            const isTrainer = req.user.isTrainer;
            const isAdmin = req.user.isAdmin;
            const isUser = !isTrainer && !isAdmin;

            // Retrieve all the possible notifications type
            const allNotificationClause = {
                userType: UserType.ALL,
            };
            const roleClause = {
                userType: isTrainer ? UserType.TRAINER : (isAdmin ? UserType.ADMIN : UserType.USER),
            };
            const idClause = {
                receiverId: new Types.ObjectId(userId),
            };

            // Remove the user deleted ones
            const deletedByClause = {
                deletedBy: {
                    $nin: new Types.ObjectId(userId),
                },
            };

            const query = {
                $or: [
                    allNotificationClause,
                    roleClause,
                    idClause,
                ],
            };
            // console.log(query);

            // Retrieving the user notifications
            Notification.find({
                $and: [
                    query,
                    deletedByClause,
                ],
            }).then((notifications: Notification[]) => {
                res.status(200).send(notifications);
            })
                .catch((err) => {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Error while retrieving the user notifications',
                    });
                });
        };
    }

    public static getNotifications() {
        return (req: Request, res: Response) => {
            Notification.find((err: any, notifications: Notification[]) => {
                if (err) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Notifications not found',
                    });
                    return;
                }

                res.status(200).send(notifications);
            });
        };
    }

    public static createNotification() {
        return (req: Request, res: Response, next: NextFunction) => {
            if (!req.user) {
                // console.log('User info not passed to createNotification()');
                res.status(404).send({
                    description: 'User not found, the requested _id seems incorrect',
                });
                return;
            }
            // console.log(req.user);
            // Retrieving user id
            const userId = req.user._id;

            // console.log(req.body);
            if (!req.body || !req.body.receiverId) {
                res.status(404).send({
                    description: 'Must specify a receiverId',
                });
                return;
            }

            const now = new Date();
            const notification = new Notification({
                notificationType: req.body.notificationType,
                userType: req.body.userType,
                receiverId: new Types.ObjectId(req.body.receiverId),
                title: req.body.title,
                description: req.body.description,
                date: now,
                userId: new Types.ObjectId(req.body.userId),
                planId: new Types.ObjectId(req.body.planId),
                readBy: [],
                deletedBy: [],
            });

            notification.save((err: any, savedNotification: Notification) => {
                if (err) {
                    res.status(404).send({
                        description: 'Error while saving the notification',
                    });
                    return;
                }

                res.status(201).send(savedNotification);
            });
        };
    }

    public static postSetNotificationsReadStatus() {
        return (req: Request, res: Response) => {
            if (!req.user) {
                // console.log('User info not passed to postSetNotificationsReadStatus()');
                res.status(404).send({
                    description: 'User not found, the requested _id seems incorrect',
                });
                return;
            }

            if (!req.body || !req.body.notifications) {
                res.status(404).send({
                    description: 'Body was empty, must set notifications fields',
                });
                return;
            }

            // Retrieving user id
            const userId = req.user._id;
            const notificationsIds: any[] = req.body.notifications;
            // console.log(notificationsIds);

            if (notificationsIds.length === 0) {
                res.status(200).send({
                    description: 'Notifications array was empty, so no notification has been updated',
                });
                return;
            }

            Notification.updateMany({
                _id: { $in: notificationsIds },
                readBy: {
                    $nin: new Types.ObjectId(userId),
                },
            }, {
                $addToSet: {
                    readBy: new Types.ObjectId(userId),
                },
            }, {
                multi: true,
            }, (err: any, notifications: any) => {
                if (err) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Notifications not found',
                    });
                    return;
                }

                // console.log(notifications);

                res.status(200).send({
                    description: 'The user has read ' + notifications.nModified + ' notifications',
                });

            });
        };
    }

    public static postDeleteNotificationForUser() {
        return (req: Request, res: Response) => {
            if (!req.user) {
                // console.log('User info not passed to postDeleteNotificationForUser()');
                res.status(404).send({
                    description: 'User not found, the requested _id seems incorrect',
                });
                return;
            }

            if (!req.body || !req.body.notifications) {
                res.status(404).send({
                    description: 'Body was empty, must set notifications fields',
                });
                return;
            }

            // Retrieving user id
            const userId = req.user._id;
            const notificationsIds: any[] = req.body.notifications;
            // console.log(notificationsIds);

            if (notificationsIds.length === 0) {
                res.status(200).send({
                    description: 'Notifications array was empty, so no notification has been deleted for the user',
                });
                return;
            }

            Notification.updateMany({
                _id: { $in: notificationsIds },
                deletedBy: {
                    $nin: new Types.ObjectId(userId),
                },
            }, {
                $addToSet: {
                    deletedBy: new Types.ObjectId(userId),
                },
            }, {
                multi: true,
            }, (err: any, notifications: any) => {
                if (err) {
                    // console.log(err);
                    res.status(404).send({
                        description: 'Notifications not found',
                    });
                    return;
                }

                // console.log(notifications);

                res.status(200).send({
                    description: 'The user has deleted ' + notifications.nModified + ' notifications',
                });

            });
        };
    }

    private static serverIO: socketIO.Server;

}
