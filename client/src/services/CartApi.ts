import Api from '@/services/Api';
import CartProduct from '@/models/CartProduct';

export class CartProductUtils {
  public static getDiscountedPrice(cartProd: CartProduct): number {
    return CartProductUtils.isOnSale(cartProd) ? 
            cartProd.product.discountedPrice * cartProd.quantity : 
            CartProductUtils.getFullPrice(cartProd);
  }
  public static getFullPrice(cartProd: CartProduct): number {
    return cartProd.product.price * cartProd.quantity;
  }
  public static isOnSale(cartProd: CartProduct): boolean {
    return cartProd.product.onSale;
  }
}

export default {
  getUserCart() {
    return Api.withAuthorization().get('/api/shop/user-cart');
  },
  postUpdateCartProducts(selectedProdId: string, updatedQuantity: number) {
    return Api.withAuthorization().post('/api/shop/update-cart-product', {
      productId: selectedProdId,
      quantity: updatedQuantity,
    },
    );
  },
  deleteUserCart() {
    return Api.withAuthorization().delete('/api/shop/delete-user-cart');
  },
};
