import { Application } from 'express';
import AchievementController from '@/controllers/AchievementController';

export default abstract class AchievementRouter {
	public static setupRoutes(app: Application) {
        app.route('/api/achievements')
            .get(AchievementController.getAchievements())
            .post(AchievementController.createAchievement());

        app.route('/api/achievements/:achievementId')
            .put(AchievementController.updateAchievement())
            .delete(AchievementController.deleteAchievement());
    }
}
