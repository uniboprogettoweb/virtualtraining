import Exercise from './Exercise';

export default interface Product {
    productId: string;
    name: string;
    categoryId: number;
    description: string;
    imageOne: string;
    imageTwo: string;
    price: number;
    discountedPrice: number;
    onSale: boolean;
    exercises: Exercise[];
  }
