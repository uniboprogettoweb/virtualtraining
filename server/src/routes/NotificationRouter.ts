import { Application } from 'express';
import NotificationController from '@/controllers/NotificationController';
import AuthMiddleware from '@/controllers/AuthMiddleware';

export default abstract class NotificationRouter {
	public static setupRoutes(app: Application, server: any) {

        // Init websockets
        NotificationController.initWebsocketIO(server);

        app.route('/api/notifications-test')
            .get(NotificationController.sendTestNotification());

        app.route('/api/notifications')
            .get(NotificationController.getNotifications())
            .post(AuthMiddleware.validateUser(), NotificationController.createNotification());

        app.route('/api/user-notifications')
            .get(AuthMiddleware.validateUser(), NotificationController.getUserNotifications())
            .post(AuthMiddleware.validateUser(), NotificationController.postSetNotificationsReadStatus())
            .delete(AuthMiddleware.validateUser(), NotificationController.postDeleteNotificationForUser());

    }
}
