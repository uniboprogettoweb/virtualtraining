import Api from '@/services/Api';
import { Muscle } from '@/services/UserRequestApi';


export interface NewRequest {
  trainerId: string;
  timetables: string;
  description: string;
  muscles: number[];
  trainerTags: number[];
}

export interface ExerciseCompleted {
  exerciseId: number;
  amount: number;
}

export default {
  getPlan() {
    return Api.withAuthorization().get('/api/users/plans');
  },
  getCard() {
    return Api.withoutAuthorization().get('/api/card');
  },
	postReview(rating: number, planId: string) {
    return Api.withAuthorization().post('/api/users/rate-plan', {rating, planId});
  },
	postRequestPlan(req: NewRequest) {
    return Api.withAuthorization().post('/api/users/request-plan', req);
  },
  postCompletePlan(exercises: ExerciseCompleted[], newExperience: number, newLevel: number) {
    return Api.withAuthorization().post('/api/users/complete-exercise', {exercises, newExperience, newLevel});
  },
  getRecommendedProducts(req: number[]) {
    return Api.withAuthorization().get('/api/shop/products/recommended', {
      params: {
        exercises: JSON.stringify(req),
      },
    });
  },
};
