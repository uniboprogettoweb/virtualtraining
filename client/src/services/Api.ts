import axios from 'axios';
import Vue from 'vue';

class Api extends Vue {
  private readonly ADMIN_PRIVILEGES = 'admin-privileges';
  private readonly TRAINER_PRIVILEGES = 'trainer-privileges';
  private readonly USER_PRIVILEGES = 'user-privileges';
  private readonly GUEST_PRIVILEGES = 'guest-privileges';
  private readonly TOKEN_NOT_FOUND = 'token_not_found';

  // Request as guest user
  public withoutAuthorization() {
    return this.performRequest();
  }
  // Request as logged user
  public withAuthorization() {
      return this.performRequest(true, this.USER_PRIVILEGES);
  }
  // Request as trainer
  public withTrainerAuthorization() {
    return this.performRequest(true, this.TRAINER_PRIVILEGES);
  }
  // Request as admin
  public withAdminAuthorization() {
    return this.performRequest(true, this.ADMIN_PRIVILEGES);
  }

  // Check if exists the admin token and if exist return it
  private getAdminAuthToken() {
    return this.$cookies.isKey('adminIsLogged') ? this.$cookies.get('adminIsLogged') : this.TOKEN_NOT_FOUND;
  }
  // Check if exists the trainer token and if exist return it
  private getTrainerAuthToken() {
    return this.$cookies.isKey('coachIsLogged') ? this.$cookies.get('coachIsLogged') : this.TOKEN_NOT_FOUND;
  }
  // Check if exists the user token and if exist return it
  private getUserAuthToken() {
    return this.$cookies.isKey('userIsLogged') ? this.$cookies.get('userIsLogged') : this.TOKEN_NOT_FOUND;
  }

  private performRequest(isAuth: boolean = false, privileges: string = this.GUEST_PRIVILEGES) {
    let token: string;
    // Get right token
    switch (privileges) {
      case this.USER_PRIVILEGES:
        token = this.getUserAuthToken();
        break;
      case this.TRAINER_PRIVILEGES:
        token = this.getTrainerAuthToken();
        break;
      case this.ADMIN_PRIVILEGES:
        token = this.getAdminAuthToken();
        break;
      default:
        token = '';
    }

    if (token === this.TOKEN_NOT_FOUND) {
      console.log('Error! You don\'t have permission to perform this request! Please login with the right account! [Api.ts]');
    }

    const definedHeaders = (token.length > 0 && token !== this.TOKEN_NOT_FOUND) ? {'Authorization' : `${token}`} : {};
    
    return axios.create({
      baseURL: `http://localhost:8081`,
      headers: definedHeaders,
    });
  }
}

export default new Api();
