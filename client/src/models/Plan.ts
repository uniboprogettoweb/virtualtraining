import Muscle from './Muscle';
import Schedule from './Schedule';

export default interface Plan {
  __v: number;
  _id: string;
  approved: boolean;
  associatedTo: string;
  compiledBy: string;
  description: string;
  isRated: boolean;
  muscles: Muscle[];
  pending: boolean;
  rating: number;
  schedules: Schedule[];
  trainerTags: number[];
}
