/**
 * Product category
 */
export default interface Category {
    categoryId: number;
    name: string;
}
