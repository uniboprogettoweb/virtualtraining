import Api from '@/services/Api';

export interface Info {}

export interface InfoPsw extends Info {
  oldPassword: string;
  password: string;
}

export interface InfoImage extends Info {
  profileImage: string;
}

export interface InfoAddress extends Info {
  address: string;
}
export interface InfoPswAddr extends Info {
  oldPassword: string;
  password: string;
	address: string;
}
export interface InfoImage extends Info {
  profileImage: string;
}

export default {
	getUserInfo() {
		return Api.withAuthorization().get('/api/users/info');
	},
  getLeaderboard() {
    return Api.withoutAuthorization().get('/api/leaderboard');
  },
	postInfo(info: Info) {
    return Api.withAuthorization().post('/api/users/info', info);
  },
};
