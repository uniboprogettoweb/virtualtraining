export interface FilterOption {
    value: string;
    text: string;
}
interface ElementWithName {
    name: string,
}
interface ElementWithEmail {
    email: string,
}
interface ElementWithFirstName {
    firstName: string,
}
interface ElementWithLastName {
    lastName: string,
}
interface ElementWithLevel {
    level: number,
}
interface ElementWithAddress {
    address: string,
}

export default class CrudUtils {
    public static getFilterOptions(fieldsName: string[]): FilterOption[] {
        let filterOptions: FilterOption[] = []
        fieldsName.forEach((name) => {
            filterOptions.push({
                value: name,
                text: 'Filter by ' + name + ':',
            })
        });
        return filterOptions;
    }

    public static compareStringAsc(stringA: string, stringB: string) {
        var a=stringA.toLowerCase(), b=stringB.toLowerCase()
        if (a < b) //sort string ascending
            return -1; 
        if (a > b)
            return 1;
        return 0; //default return value (no sorting)
    }

    public static compareBooleanAsc(boolA: boolean, boolB: boolean): number {
        return CrudUtils.compareNumberAsc(boolA ? 1 : 0, boolB ? 1 : 0);
    }

    public static compareNumberAsc(numA: number, numB: number): number {
        return numA - numB;
    }

    public static compareNumberDesc(numA: number, numB: number): number {
        return numB - numA;
    }

    public static compareDateAsc(dateA: Date, dateB: Date) {
        if (dateA < dateB) //sort date ascending
            return -1; 
        if (dateA > dateB)
            return 1;
        return 0;
    }

    public static searchByName(collection: ElementWithName[], searchKey: string): ElementWithName[] {
        let filtered: ElementWithName[] = [];
        const searchUpper = searchKey.toUpperCase();
        collection.forEach(elem => {
        const elemToUpp = elem.name.toUpperCase();
        if (elemToUpp.includes(searchUpper)) {
            filtered.push(elem);
        }
        });
        return filtered;
    }

    public static searchByFirstName(collection: ElementWithFirstName[], searchKey: string): ElementWithFirstName[] {
        let filtered: ElementWithFirstName[] = [];
        const searchUpper = searchKey.toUpperCase();
        collection.forEach(elem => {
        const elemToUpp = elem.firstName.toUpperCase();
        if (elemToUpp.includes(searchUpper)) {
            filtered.push(elem);
        }
        });
        return filtered;
    }
    public static searchByLastName(collection: ElementWithLastName[], searchKey: string): ElementWithLastName[] {
        let filtered: ElementWithLastName[] = [];
        const searchUpper = searchKey.toUpperCase();
        collection.forEach(elem => {
        const elemToUpp = elem.lastName.toUpperCase();
        if (elemToUpp.includes(searchUpper)) {
            filtered.push(elem);
        }
        });
        return filtered;
    }
    public static searchByAddress(collection: ElementWithAddress[], searchKey: string): ElementWithAddress[] {
        let filtered: ElementWithAddress[] = [];
        const searchUpper = searchKey.toUpperCase();
        collection.forEach(elem => {
        const elemToUpp = elem.address.toUpperCase();
        if (elemToUpp.includes(searchUpper)) {
            filtered.push(elem);
        }
        });
        return filtered;
    }
    public static searchByLevel(collection: ElementWithLevel[], searchKey: string): ElementWithLevel[] {
        let filtered: ElementWithLevel[] = [];
        collection.forEach(elem => {
            if(elem.level == undefined) elem.level = 0; 
            if (elem.level.toString().includes(searchKey)) filtered.push(elem);
        });
        return filtered;
    }
    public static searchByEmail(collection: ElementWithEmail[], searchKey: string): ElementWithEmail[] {
        let filtered: ElementWithEmail[] = [];
        const searchUpper = searchKey.toUpperCase();
        collection.forEach(elem => {
        const elemToUpp = elem.email.toUpperCase();
        if (elemToUpp.includes(searchUpper)) {
            filtered.push(elem);
        }
        });
        return filtered;
    }

    public static checkTextMaxLength(text: string, maxLength: number): string {
        if(text == undefined) return '';
        return text.length > maxLength ? 
                text.substring(0, maxLength) + '...' : 
                text;
    }
}