import Api from '@/services/Api';

export interface TrainerAppr {
  trainerId: string;
  status: boolean;
}

export interface PlanAppr {
  planId: string;
  status: boolean;
}

export default {
  getPendingPlans() {
    return Api.withAdminAuthorization().get('/api/admin/pending-plans');
  },

  getPendingTrainers() {
    return Api.withAdminAuthorization().get('/api/admin/pending-trainers');
  },

  postValidateTrainer(decision: TrainerAppr) {
    return Api.withAdminAuthorization().post('/api/admin/validate-trainer', decision);
  },

  postValidatePlan(decision: PlanAppr) {
    return Api.withAdminAuthorization().post('/api/admin/validate-plan', decision);
  },
};
