import { Document, Schema, Model, model } from 'mongoose';
import { BaseUser } from '@/models/User';

export interface Admin extends BaseUser {
    firstName: string;
	lastName: string;
}

const AdminSchema: Schema = new Schema({
	email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    firstName: { type: String, required: true },
	lastName: { type: String, required: true },
	isTrainer: { type: Boolean, required: true, default: false },
	isAdmin: { type: Boolean, required: true, default: true },
	salt: { type: String, required: true},
	createdAt: Date,
	updatedAt: Date,
	tokens: { type: Array , required: true, default: [] },
});

// Update time related fields
AdminSchema.pre<Admin>('save', function(next) {
	if (this) {
		const now = new Date();
		if (!this.createdAt) {
			this.createdAt = now;
		}
		this.updatedAt = now;
	}
	next();
});

// Export the Admin model and return the Admin interface
export const Admin: Model<Admin> = model<Admin>('Admin', AdminSchema);
