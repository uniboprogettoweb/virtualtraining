import Api from '@/services/Api';


export default {
  getAchievement() {
    return Api.withoutAuthorization().get('/api/achievements');
  },
  complete() {
    return Api.withAuthorization().post('/api/users/complete-exercise');
  },
};
