import { Document, Schema, Model, model } from 'mongoose';
import { Card } from './Card';

export interface Schedule extends Document {
    cards: object[];
}

const ScheduleSchema: Schema = new Schema({
    cards: [{ type: Schema.Types.Mixed, required: true }],
});

// Export the Schedule model and return the Schedule interface
// tslint:disable-next-line: max-line-length
export const Schedule: Model<Schedule> = model<Schedule>('Schedule', ScheduleSchema);
